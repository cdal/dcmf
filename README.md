# dCMF #

[Deep Collective Matrix Factorization for Augmented Multi-View Learning](https://arxiv.org/abs/1811.11427). This repository contains source code for reproducing the experimental results.

For dCMF of an arbitrary collection of matrices: 
[https://bitbucket.org/mragunathan/dcmf_generic](https://bitbucket.org/mragunathan/dcmf_generic)

## Prerequisites
- Python 3.6 
- PyTorch 0.4.0
- NumPy 1.14.0
- SciPy 1.0.0
- scikit-learn 0.19.1
- GPy 1.9.6 (modified to support MTBO)
	- Steps to setup  
		`git clone https://github.com/mragunathan/GPy.git`  
		`cd GPy`  
		`git checkout devel`  
		`python setup.py build_ext --inplace`  
		`nosetests GPy/testing`  
		`python setup.py install`  
- GPyOpt 1.2.5 (modified to support MTBO)
	- Steps to setup  
		`git clone https://github.com/mragunathan/GPyOpt.git`  
		`cd GPyOpt`  
		`python setup.py develop`  

### Running dCMF experiments ###

* Download the data from the [link here](https://drive.google.com/open?id=1jbExiYuKl0W-MrdzoBSiLnlPSE0eKbQb) and untar it to DCMF_DIR/data directory
* Run the main_* file in the corresponding experiment folders.


