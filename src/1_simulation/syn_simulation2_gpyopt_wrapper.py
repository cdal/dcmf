import numpy as np
#
import GPy
import GPyOpt
from numpy.random import seed
#
from hyperopt import mtgp
from hyperopt import objective_multi_output
from hyperopt import AcquisitionEI_multi_output

import deepcmf_syn_sim2_gpyopt_wrapper as bb_wrapper
#import deepcmf_syn_sim2_wrapper as syn_sim2_wrapper

import pprint

# def get_temp(num_folds):
#     dict_params_run_rmse = {}
#     # sample value
#     cur_rmse_train = 0
#     cur_sd_train = 0
#     cur_rmse_test = 0
#     cur_sd_test = 0
#     dict_params = {"learning_rate":1e-6,\
#                 "conv_threshold":1e-4,\
#                 #"#conv_threshold":random.uniform(1e-5,1e-4),\
#                 "weight_decay":0.05,\
#                 #"weight_decay":random.uniform(0.05,0.5),\
#                 "kf":0.1,\
#                 #"kf":random.uniform(0.1,0.5),\ 
#                 "num_batches":1,\
#                 "max_epochs":500}
#     #
#     for i in np.arange(num_folds):
#         dict_params_run_rmse[i] = {"rmse_train":cur_rmse_train,"sd_train":cur_sd_train,"rmse_test":cur_rmse_test,"sd_test":cur_sd_test,"params":dict_params}
#     return dict_params_run_rmse

def best_value(X,Y,rmse_dict_list):
    temp_rmse_list = []
    for i in np.arange(len(rmse_dict_list)):
        temp_rmse_list.append(rmse_dict_list[i]["avg_rmse_test"])
    best_idx = np.argmin(np.array(temp_rmse_list))
    best_X = X[best_idx]
    best_Y = Y[best_idx]
    best_rmse_dict = rmse_dict_list[best_idx]
    return best_X,best_Y,best_rmse_dict

# def best_value(X,Y,sign=1):
#     Y_norm = np.atleast_2d(np.sum(Y,axis=1)).T
#     if sign == 1:
#         best_idx = np.argmin(Y_norm)
#     else:
#         best_idx = np.argmax(Y_norm)
#     best_X = X[best_idx]
#     best_Y = Y[best_idx]
#     return best_X,best_Y    

def dcmf_bo(cur_run_data_dir,num_params_samples,num_folds,model_type,w_pool):
    print("##################")
    print("BO - start")
    print("cur_run_data_dir: ",cur_run_data_dir)
    print("num_params_samples: ",num_params_samples)
    print("num_folds: ",num_folds)
    print("model_type: ",model_type)
    print("#")
    #
    bb = bb_wrapper.deepcmf_gpyopt(cur_run_data_dir,num_folds)
    #
    bounds = [
    {'name': 'learning_rate', 'type': 'continuous', 'domain': (1e-6,1e-4)},\
    {'name': 'conv_threshold', 'type': 'continuous', 'domain': (1e-5,1e-4)},\
    {'name': 'weight_decay', 'type': 'continuous', 'domain': (0.05,0.5)},\
    {'name': 'kf', 'type': 'continuous', 'domain': (0.1,0.5)}
    ]
    #
    if model_type == "gp":
        print("# using gp objective")
        objective = objective_multi_output.SingleObjectiveMultiOutput(bb.run_dcmf_gp)
        model = mtgp.MTGPModel(num_params_samples)
    else:
        print("# using mtgp objective")
        objective = objective_multi_output.SingleObjectiveMultiOutput(bb.run_dcmf)
        if w_pool:
            print("# W pooling")
            W = None
        else:
            print("# W from graph")
            W = np.array([[1.41421356, 0.        , 0.        , 0.        , 0.70710678,
                0.        , 0.70710678],
               [0.        , 1.41421356, 0.        , 0.        , 0.        ,
                0.70710678, 0.70710678],
               [0.        , 0.        , 1.41421356, 0.        , 0.70710678,
                0.        , 0.        ],
               [0.        , 0.        , 0.        , 1.41421356, 0.        ,
                0.70710678, 0.        ],
               [0.        , 0.        , 0.        , 0.        , 1.        ,
                0.        , 0.5       ],
               [0.        , 0.        , 0.        , 0.        , 0.        ,
                1.        , 0.5       ],
               [0.        , 0.        , 0.        , 0.        , 0.        ,
                0.        , 0.70710678]])
        #print("# W - pooled")
        #model = mtgp.MTGPModel(num_params_samples) #W - pooled
        #print("# W - from graph")
        model = mtgp.MTGPModel(num_params_samples,W) #W - from graph
    #
    space = GPyOpt.Design_space(space=bounds)
    #
    #
    aquisition_optimizer = GPyOpt.optimization.AcquisitionOptimizer(space)
    #
    initial_design = GPyOpt.experiment_design.initial_design('random', space, 5)
    #
    acquisition = AcquisitionEI_multi_output.AcquisitionEIMO(model, space, optimizer=aquisition_optimizer)
    #
    evaluator = GPyOpt.core.evaluators.Sequential(acquisition)
    #
    bo = GPyOpt.methods.ModularBayesianOptimization(model, space, objective, acquisition, evaluator, initial_design, normalize_Y=False)
    bo.run_optimization(max_iter=num_params_samples, verbosity=True, eps = 0)
    #
    #get best x,y
    x_best, y_best,rmse_best_dict = best_value(bo.X,bo.Y,bo.rmse_dict_list)
    print("#BO - end")
    print("##################")
    print("x_best: ")
    print(x_best)
    print("x_best details: ")
    x_params_list = x_best
    learning_rate = x_params_list[0]
    conv_threshold = x_params_list[1]
    weight_decay = x_params_list[2]
    kf = x_params_list[3]
    #
    dict_params = {"learning_rate":learning_rate,\
            "conv_threshold":conv_threshold,\
            "weight_decay":weight_decay,\
            "kf":kf,\
            "num_batches":1,\
            "max_epochs":200}
    pp = pprint.PrettyPrinter()
    pp.pprint(dict_params)           
    print("y_best: ")
    pp.pprint(y_best)
    print("rmse_best_dict:")
    pp.pprint(rmse_best_dict)
    print("#")
    # avg_rmse_train,sd_train,avg_rmse_test,sd_test,all_losses = syn_sim2_wrapper.run_deepcmf_syn_simulation_2(cur_run_data_dir,dict_params,num_folds)
    # print("run_dcmf - result: ")
    # pp = pprint.PrettyPrinter()
    # print("dict_params: ")
    # pp.pprint(dict_params)
    # print("all_losses: ")
    # pp.pprint(all_losses)
    # print("perf: (check12)")
    # print("avg_rmse_train: ",avg_rmse_train)
    # print("sd_train: ",sd_train)
    # print("avg_rmse_test: ",avg_rmse_test)
    # print("sd_test: ",sd_test)
    # print("#")
    # #construct dict_params_run_rmse and return
    # dict_params_run_avg_rmse = {"rmse_train":avg_rmse_train,"sd_train":sd_train,"rmse_test":avg_rmse_test,"sd_test":sd_test,"params":dict_params}
    #return dict_params_run_avg_rmse
    rmse_best_dict["params"] = dict_params
    return rmse_best_dict