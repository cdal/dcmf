import logging

#Synthetic data generator
working_dir_path ="."
log_dir = working_dir_path + "/"
log_format = "%(asctime)s %(filename)s %(lineno)d %(levelname)s %(message)s"
log_level = logging.DEBUG
