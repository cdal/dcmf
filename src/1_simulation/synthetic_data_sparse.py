import numpy as np
import pandas as pd
import logging
import config.config as cfg
import os
from datetime import datetime
import time
import random
from sklearn.preprocessing import MaxAbsScaler

class SyntheticDataGeneratorSparse(object):
    def __init__(self,num_entities,entity_size_list,k,entity_relation_dict,sparsity_percentage,sparse_entities_list=None):
        #log
        logging.basicConfig(filename=cfg.log_dir+\
            os.sep+\
            __name__+\
            "_"+\
            datetime.now().strftime('%Y_%m_%d_%H_%M_%S.log')\
            ,format=cfg.log_format,level=cfg.log_level)
        self.logger = logging.getLogger(object.__name__)
        #params
        self.num_entities = num_entities
        self.entity_size_list = entity_size_list
        self.k = k
        self.entity_relation_dict = entity_relation_dict
        self.sparsity_percentage = sparsity_percentage
        if sparse_entities_list == None:
            self.sparse_entities_list = list(self.entity_relation_dict.keys())
        else:
            self.sparse_entities_list = sparse_entities_list
        self.logger.debug("Sparse ynthetic data generator initiated.")

    def __get_view_matrices_from_entity_factors(self,entity_factors_dict,entity_relation_dict):
        self.logger.debug("Generating view matrices...")
        view_id = 0
        view_matrices_dict = {}
        for row_entity_id in entity_relation_dict.keys():
            for col_entity_id in entity_relation_dict[row_entity_id]:
                s = time.time() 
                cur_view_mat = np.dot(entity_factors_dict["Ue"+str(row_entity_id)],entity_factors_dict["Ue"+str(col_entity_id)].T)
                view_matrices_dict["X_"+str(row_entity_id)+str(col_entity_id)] = MaxAbsScaler().fit_transform(cur_view_mat)
                e = time.time()
                self.logger.debug("view: %s shape: %s x %s. Took %s mins.",view_id+1,cur_view_mat.shape[0],cur_view_mat.shape[1],round((e-s)/60.0,2))
                view_id+=1
        return view_matrices_dict

    def __generate_random_nn_entity_factor_matrices(self,num_entities,entity_size_list,k,sp_per): 
        self.logger.debug("Generating factor matrices...")
        entity_factors_dict = {}
        for entity_id in np.arange(1,num_entities+1):
            num_rows = entity_size_list[entity_id-1]
            num_cols = k
            s = time.time()
            #if entity_id == 0 or entity_id == 1:
            if str(entity_id) in self.sparse_entities_list:
                temp_u = np.random.random((num_rows,num_cols))
                rows_idx = random.sample(list(np.arange(num_rows)),int(num_rows * (sp_per/100.0)))
                #cols_idx = random.sample(list(np.arange(num_cols)),int(num_cols * (sp_per/100.0)))
                temp_u[rows_idx,:] = 0
                #temp_u[:,cols_idx] = 0
                entity_factors_dict["Ue"+str(entity_id)] = temp_u
            else:    
                entity_factors_dict["Ue"+str(entity_id)] = np.random.random((num_rows,num_cols))
            e = time.time()
            self.logger.debug("entity_id: %s shape: %s x %s. Took %s mins.",entity_id,num_rows,num_cols,round((e-s)/60.0,2))
        return entity_factors_dict

    def get_random_non_negative_view_matrices(self):
        self.logger.debug("num_entities: %s",self.num_entities)
        self.logger.debug("entity_size_list: %s",self.entity_size_list)
        self.logger.debug("k: %s",self.k)
        self.logger.debug("entity_relation_dict: %s",self.entity_relation_dict)
        entity_factors_dict = self.__generate_random_nn_entity_factor_matrices(self.num_entities,self.entity_size_list,self.k,self.sparsity_percentage)
        print("entity_factors_dict.keys(): ",entity_factors_dict.keys())
        view_matrices_dict = self.__get_view_matrices_from_entity_factors(entity_factors_dict,self.entity_relation_dict)
        return view_matrices_dict,entity_factors_dict
