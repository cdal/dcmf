from synthetic_data_sparse import SyntheticDataGeneratorSparse
from scipy.sparse import csr_matrix
import numpy as np

#sparse
num_entities = 4
entity_size_list = [100,200,400,500]
k = 100
entity_relation_dict = {"1":["2","3"],"2":["1"],"3":["1"],"4":["2"]}
sparse_percent = 10 #percentage of zeros
sparse_entities_list = ["1"] #matrices associated with these entities will be sparse. If None, all matrices will be sparse.

#dense
#For dense matrices, set sparse_percent = 0

data_generator = SyntheticDataGeneratorSparse(num_entities,entity_size_list,k,entity_relation_dict,sparse_percent,sparse_entities_list)
dict_X, dict_U = data_generator.get_random_non_negative_view_matrices()

for cur_x_id in dict_X.keys():
	cur_x = dict_X[cur_x_id]
	print("cur_x_id:",cur_x_id,", cur_x.shape: ",cur_x.shape,", %nz: ",np.round(100 * csr_matrix(cur_x).nnz/np.prod(cur_x.shape),4))

print("#")

for cur_u_id in dict_U.keys():
	cur_u = dict_U[cur_u_id]
	print("cur_u_id:",cur_u_id,", cur_u.shape: ",cur_u.shape,", %nz: ",np.round(100 * csr_matrix(cur_u).nnz/np.prod(cur_u.shape),4))
