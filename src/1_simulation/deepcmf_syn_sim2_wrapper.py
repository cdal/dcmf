import pandas as pd
import numpy as np
import time
#
import torch
from torch.autograd import Variable
from torch.nn import Tanh, Sigmoid, ReLU, LeakyReLU
from torch import nn
#
from aec import autoencoder
#
from deepcmf_syn_sim2 import deepcmf
#
import pickle as pkl
#
from sklearn import preprocessing
import time
#
import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt


def run_deepcmf_syn_simulation_2(cur_run_data_dir,dict_params,num_train_test_splits):
    #
    learning_rate = dict_params['learning_rate']
    convg_thres = dict_params['conv_threshold']
    weight_decay = dict_params['weight_decay']
    kf = dict_params['kf']
    num_chunks = dict_params['num_batches']
    max_epochs = dict_params['max_epochs']
    #
    k = 100 
    e_actf = 'tanh'
    d_actf = 'tanh'
    #
    isPretrain = False
    pretrain_thres = 1e-3
    #
    num_epochs = None
    #
    data_dir = cur_run_data_dir
    #out_dir = '/data/deepcmf_repo/out/syn_data/exp1/'
    #
    is_debug = False
    #
    total_num_folds = num_train_test_splits
    #debug
    print("current run params:")
    print("#")
    print("learning_rate: ",learning_rate)
    print("convg_thres: ",convg_thres)
    print("weight_decay: ",weight_decay)
    print("kf: ",kf)
    print("num_chunks: ",num_chunks)
    print("#")
    print("k: ",k)
    print("e_actf: ",e_actf)
    print("d_actf: ",d_actf)
    print("#")
    print("isPretrain: ",isPretrain)
    print("pretrain_thres: ",pretrain_thres)
    print("#")
    print("num_epochs: ",num_epochs)
    print("max_epochs: ",max_epochs)
    print("total_num_folds: ",total_num_folds)
    print("#")
    print("data_dir: ",data_dir)
    #print("out_dir: ",out_dir)
    print("#")
    #
    #load side matrices
    orgU1 = pkl.load(open(data_dir+"X_13.pkl",'rb'))
    orgV1 = pkl.load(open(data_dir+"X_42.pkl",'rb'))
    #
    if is_debug:
        print("orgU1.shape: ",orgU1.shape)
        print("orgV1.shape: ",orgV1.shape)
    #
    # Uscaler = preprocessing.MaxAbsScaler()
    # U1 = Uscaler.fit_transform(orgU1)
    # Vscaler = preprocessing.MaxAbsScaler()
    # V1 = Vscaler.fit_transform(orgV1)
    U1 = orgU1
    V1 = orgV1
    #
    if is_debug:
        print("U1.shape: ",U1.shape)
        print("V1.shape: ",V1.shape)
        print("#")
        print("U1 - min: ",np.min(U1),", max: ", np.max(U1),", med: ", np.median(U1))
        print("V1 - min: ",np.min(V1),", max: ", np.max(V1),", med: ", np.median(V1))
        print("#")
    #
    run_name = "syn_deepcmf_exp1_"
    #
    dict_fold_rmse_test = {}
    dict_fold_rmse_train = {}
    dict_fold_losses = {}
    for fold_num in np.arange(1,total_num_folds+1):
        dict_epochs_cost = {}
        #
        if is_debug:
            print("fold#: ",fold_num)
            print("###")
        #central matrix fold 
        orgRtrain = pkl.load(open(data_dir+'/X_12_train_fold_'+str(fold_num)+'.pkl','rb'))
        #Rscaler = preprocessing.MaxAbsScaler()
        #Rtrain = Rscaler.fit_transform(orgRtrain)
        Rtrain = orgRtrain
        Rtrain_idx = pkl.load(open(data_dir+'/X_12_train_idx_'+str(fold_num)+'.pkl','rb')) 
        Rtest = pkl.load(open(data_dir+'/X_12_test_fold_'+str(fold_num)+'.pkl','rb'))
        Rtest_idx = pkl.load(open(data_dir+'/X_12_test_idx_'+str(fold_num)+'.pkl','rb'))
        Rdoublets = pkl.load(open(data_dir+'/R_doublets_'+str(fold_num)+'.pkl','rb'))
        if is_debug:
            print("Rtrain.shape: ",Rtrain.shape)
            print("Rtest.shape: ",Rtest.shape)
            print("len(Rtest_idx): ",len(Rtest_idx))
            print("Rdoublets.shape: ",Rdoublets.shape)
            print("Rtrain - min: ",np.min(Rtrain),", max: ", np.max(Rtrain),", med: ", np.median(Rtrain))
        #
        dcmf = deepcmf(learning_rate=learning_rate,\
                        num_epochs=num_epochs,\
                        convg_thres=convg_thres,\
                        num_chunks=num_chunks,\
                        k=k,\
                        kf=kf,\
                        e_actf=e_actf,\
                        d_actf=d_actf,\
                        isPretrain=isPretrain,\
                        pretrain_thres=pretrain_thres,\
                        weight_decay=weight_decay,\
                        max_epochs=max_epochs)
        if is_debug:
            print("fit: start")
        s = time.time()
        dcmf.fit(Rtrain,U1,V1.T)
        e = time.time()
        if is_debug:
            print("fit: end. Took ",round((e-s)/60.0,2)," mins.")
        #
        Rpred = dcmf.predict()
        #
        if is_debug:
            print("#")
            print("Rpred.shape: ",Rpred.shape)
            print("#")
            print("get_rmse - train")
        dict_fold_rmse_train[fold_num] = dcmf.get_rmse(Rpred,orgRtrain,Rtrain_idx,Rdoublets)
        if is_debug:
            print("get_rmse - test")
        dict_fold_rmse_test[fold_num] = dcmf.get_rmse(Rpred,Rtest.todense(),Rtest_idx,Rdoublets)
        if is_debug:
                print("get_losses - train")
        dict_fold_losses[fold_num] = dcmf.get_losses()
        #
        print("Result fold#: ",fold_num)
        print("RMSE test: ",dict_fold_rmse_test[fold_num])
        print("RMSE train: ",dict_fold_rmse_train[fold_num])
        print("Losses train: ",dict_fold_losses[fold_num])
        print("#")
    #
    avg_rmse_test = np.mean(list(dict_fold_rmse_test.values()))
    sd_test = np.std(list(dict_fold_rmse_test.values()))
    avg_rmse_train = np.mean(list(dict_fold_rmse_train.values()))
    sd_train = np.std(list(dict_fold_rmse_train.values()))
    #
    temp_list = []
    for temp_val in list(dict_fold_losses.values()):
        temp_list.append(list(temp_val[0]))
    temp_arr = np.array(temp_list)
    avg_losses = np.atleast_2d(np.mean(temp_arr,axis=0))
    #
    return avg_rmse_train,sd_train,avg_rmse_test,sd_test,avg_losses


