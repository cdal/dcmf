import numpy as np
import time
import os
import pandas as pd
import pprint
import syn_simulation2_gpyopt_wrapper as syn_sim2_gpyopt_wrapper

data_dir = "../../data/preprocessed/syn_data/sim2/"
out_dir = "../../out/syn/sim2/"
num_runs_idxs = [1,2,3] 
sparsity_percentage_list = [18,36,51]
#
num_runs = len(num_runs_idxs)
num_params_samples = 200
num_folds = 5
#model_type = "gp"
model_type = "mtgp"
w_pool = True

# def get_best_rmse(dict_params_run_rmse):
#     #dict_params = {"learning_rate":1e-4,"conv_threshold":1e-3,"weight_decay":0.05}
#     #dict_best_rmse = {"rmse_train":-1,"rmse_test":-1,"params":dict_params}
#     temp_list_key = []
#     temp_list_test_rmse = []
#     for run_key in dict_params_run_rmse.keys():
#         temp_list_key.append(run_key)
#         temp_list_test_rmse.append(dict_params_run_rmse[run_key]['rmse_test'])
#     max_rmse_i = np.argmin(np.array(temp_list_test_rmse))
#     max_rmse_key = temp_list_key[max_rmse_i]
#     dict_best_rmse = dict_params_run_rmse[max_rmse_key]    
#     return dict_best_rmse

#starts here...
print("model_type: ",model_type)
print("w_pool: ",w_pool)
print("num_folds: ",num_folds)
print("num_params_samples: ",num_params_samples)
print("num_runs: ",num_runs)
print("#")

pp = pprint.PrettyPrinter()
dict_run_best_rmse = {}
dict_run_all_rmse = {}
for i in num_runs_idxs: #np.arange(1,num_runs+1):
    run_s = time.time()
    print("run #",i," - start")
    print("###################")
    print("sparsity_percentage ",sparsity_percentage_list[i-1])
    cur_run_data_dir = data_dir+str(i)+'/'
    if not os.path.isdir(cur_run_data_dir):
        raise Exception("Missing data_dir: "+str(data_dir))
    print(">>> Loading data from ",cur_run_data_dir)
    print("#")
    print("Performing BO for hyperparameter selection and running dCMF.")
    ps_s = time.time()
    print("BO hyperopt run #",i," - start")
    print("-----------------------------------")        
    dict_cur_run_best_rmse = syn_sim2_gpyopt_wrapper.dcmf_bo(cur_run_data_dir,num_params_samples,num_folds,model_type,w_pool) 
                                              # return format: 
                                              # dict_params_run_rmse[j] = {"rmse_train":cur_rmse_train,"sd_train":cur_sd_train,"rmse_test":cur_rmse_test,"sd_test":cur_sd_test,"params":dict_params}
                                              # where
                                              # dict_params = {"learning_rate":1e-6,\
                                              # "conv_threshold":1e-4,\
                                              # "weight_decay":0.05,\
                                              # "kf":0.1,\
                                              # "num_batches":1,\
                                              # "max_epochs":500}
                                              # and 
                                              # j is the run idx
    ps_e = time.time()
    print("BO hyperopt run #",i," - end. Took ",round(ps_e-ps_s,2)," secs.")
    print("------------------------------------------------------------")
    #dict_cur_run_best_rmse = get_best_rmse(dict_params_run_rmse)
    print("dict_cur_run_best_rmse: ")
    pp.pprint(dict_cur_run_best_rmse)
    dict_run_best_rmse[i] = dict_cur_run_best_rmse
    #dict_run_all_rmse[i] = dict_params_run_rmse
    run_e = time.time()
    print("run #",i," - end. Took ",round(run_e-run_s,2)," secs.")
    print("###############################################")

#persist result
#pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"simulation_2_runs_"+str(num_runs_idxs)+"_params_sampling_"+str(num_params_samples)+"_best_rmse_MTGP_W_from_graph.json")
#pd.DataFrame.from_dict(dict_run_all_rmse).to_json(out_dir+"simulation_2_runs_"+str(num_runs_idxs)+"_params_sampling_"+str(num_params_samples)+"_all_rmse.json")

print("result: ")
print("###")
print("dict_run_best_rmse: ")
print("#")
pp.pprint(dict_run_best_rmse)

if w_pool:
  #pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"sim2_mtgp1_runs_"+str(num_runs)+"_num_params_samples_"+str(num_params_samples)+".json")
  pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"sim2_mtgp1.json")
else:
  pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"sim2_mtgp2_runs_"+str(num_runs)+"_num_params_samples_"+str(num_params_samples)+".json")



