
import deepcmf_syn_sim2_wrapper as syn_sim2_wrapper
import pprint
import numpy as np

class deepcmf_gpyopt():
    """
    deepcmf
    """
    def __init__(self,cur_run_data_dir,num_folds):
        self.cur_run_data_dir = cur_run_data_dir
        self.num_folds = num_folds
        self.pp = pprint.PrettyPrinter()

    def run_dcmf(self,x):
        print("####")
        print("#dcmf - start ")
        x_params_list = x[0]
        learning_rate = x_params_list[0]
        conv_threshold = x_params_list[1]
        weight_decay = x_params_list[2]
        kf = x_params_list[3]
        #
        dict_params = {"learning_rate":learning_rate,\
                "conv_threshold":conv_threshold,\
                "weight_decay":weight_decay,\
                "kf":kf,\
                "num_batches":1,\
                "max_epochs":500}
        num_train_test_splits = self.num_folds
        cur_run_data_dir = self.cur_run_data_dir
        avg_rmse_train,sd_train,avg_rmse_test,sd_test,all_losses = syn_sim2_wrapper.run_deepcmf_syn_simulation_2(cur_run_data_dir,dict_params,num_train_test_splits)
        print("run_dcmf - result: ")
        print("dict_params: ")
        self.pp.pprint(dict_params)
        print("all_losses: ")
        self.pp.pprint(all_losses)
        print("perf: ")
        print("avg_rmse_train: ",avg_rmse_train)
        print("sd_train: ",sd_train)
        print("avg_rmse_test: ",avg_rmse_test)
        print("sd_test: ",sd_test)
        print("#")
        print("#dcmf - end ")
        print("####")
        return all_losses,{"avg_rmse_train":avg_rmse_train,"sd_train":sd_train,"avg_rmse_test":avg_rmse_test,"sd_test":sd_test}

    def run_dcmf_gp(self,x):
        print("####")
        print("run_dcmf - start ")
        x_params_list = x[0]
        learning_rate = x_params_list[0]
        conv_threshold = x_params_list[1]
        weight_decay = x_params_list[2]
        kf = x_params_list[3]
        #
        dict_params = {"learning_rate":learning_rate,\
                "conv_threshold":conv_threshold,\
                "weight_decay":weight_decay,\
                "kf":kf,\
                "num_batches":1,\
                "max_epochs":500}
        num_train_test_splits = self.num_folds
        cur_run_data_dir = self.cur_run_data_dir
        avg_rmse_train,sd_train,avg_rmse_test,sd_test,all_losses = syn_sim2_wrapper.run_deepcmf_syn_simulation_2(cur_run_data_dir,dict_params,num_train_test_splits)
        print("run_dcmf - result: ")
        print("dict_params: ")
        self.pp.pprint(dict_params)
        print("all_losses: ")
        self.pp.pprint(all_losses)
        print("perf: ")
        print("avg_rmse_train: ",avg_rmse_train)
        print("sd_train: ",sd_train)
        print("avg_rmse_test: ",avg_rmse_test)
        print("sd_test: ",sd_test)
        print("#")
        print("run_dcmf - end ")
        print("####")
        return np.atleast_2d(np.sum(all_losses)),{"avg_rmse_train":avg_rmse_train,"sd_train":sd_train,"avg_rmse_test":avg_rmse_test,"sd_test":sd_test}