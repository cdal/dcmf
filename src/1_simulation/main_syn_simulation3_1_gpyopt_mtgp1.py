import numpy as np
import time
import os
import pandas as pd
import pprint
import syn_simulation2_gpyopt_wrapper as syn_sim2_gpyopt_wrapper

data_dir = "../../data/preprocessed/syn_data/sim31/"
out_dir = "../../out/syn/sim31/"
num_runs_idxs = [1,2,3] 

base_size_list = [200,400,40,80]
entity_size_list_of_lists = [list(np.array(base_size_list)*2),list(np.array(base_size_list)*4),list(np.array(base_size_list)*6),list(np.array(base_size_list)*8),list(np.array(base_size_list)*10)]

num_params_samples = 200
num_folds = 5
#model_type = "gp"
model_type = "mtgp"
w_pool = True
num_runs = len(num_runs_idxs)

#starts here...
print("model_type: ",model_type)
print("w_pool: ",w_pool)
print("num_folds: ",num_folds)
print("num_params_samples: ",num_params_samples)
print("num_runs: ",num_runs)
print("#")
pp = pprint.PrettyPrinter()
dict_run_best_rmse = {}
dict_run_all_rmse = {}
for i in num_runs_idxs: #np.arange(1,num_runs+1):
    run_s = time.time()
    print("run #",i," - start")
    print("###################")
    print("entity_size: ",entity_size_list_of_lists[i-1])
    cur_run_data_dir = data_dir+str(i)+'/'
    if not os.path.isdir(cur_run_data_dir):
        raise Exception("Missing data_dir: "+str(data_dir))
    print(">>> Loading data from ",cur_run_data_dir)
    print("#")
    print("Performing BO for hyperparameter selection and running dCMF.")
    ps_s = time.time()
    print("BO hyperopt run #",i," - start")
    print("-----------------------------------")        
    dict_cur_run_best_rmse = syn_sim2_gpyopt_wrapper.dcmf_bo(cur_run_data_dir,num_params_samples,num_folds,model_type,w_pool) 
                                              # return format: 
                                              # dict_params_run_rmse[j] = {"rmse_train":cur_rmse_train,"sd_train":cur_sd_train,"rmse_test":cur_rmse_test,"sd_test":cur_sd_test,"params":dict_params}
                                              # where
                                              # dict_params = {"learning_rate":1e-6,\
                                              # "conv_threshold":1e-4,\
                                              # "weight_decay":0.05,\
                                              # "kf":0.1,\
                                              # "num_batches":1,\
                                              # "max_epochs":500}
                                              # and 
                                              # j is the run idx
    ps_e = time.time()
    print("BO hyperopt run #",i," - end. Took ",round(ps_e-ps_s,2)," secs.")
    print("------------------------------------------------------------")
    #dict_cur_run_best_rmse = get_best_rmse(dict_params_run_rmse)
    print("dict_cur_run_best_rmse: ")
    pp.pprint(dict_cur_run_best_rmse)
    dict_run_best_rmse[i] = dict_cur_run_best_rmse
    #dict_run_all_rmse[i] = dict_params_run_rmse
    run_e = time.time()
    print("run #",i," - end. Took ",round(run_e-run_s,2)," secs.")
    print("###############################################")

#persist result
#pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"simulation_31_runs_"+str(num_runs_idxs)+"_params_sampling_"+str(num_params_samples)+"_best_rmse_MTGP_W_from_graph.json")

print("result: ")
print("###")
print("dict_run_best_rmse: ")
print("#")
pp.pprint(dict_run_best_rmse)


if w_pool:
  #pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"sim31_mtgp1_runs_"+str(num_runs)+"_num_params_samples_"+str(num_params_samples)+".json")
  pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"sim31_mtgp1.json")
else:
  pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"sim31_mtgp2_runs_"+str(num_runs)+"_num_params_samples_"+str(num_params_samples)+".json")



