import pandas as pd
import numpy as np
import time
#
import torch
from torch.autograd import Variable
from torch.nn import Tanh, Sigmoid, ReLU, LeakyReLU
from torch import nn
#
from aec import autoencoder
#
from deepcmf import deepcmf
#
import pickle as pkl
#
from sklearn import preprocessing
#
import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt

from scipy.sparse import lil_matrix

#Hyperparams
k = 200 #final encoding lenghth
kf = 0.5
e_actf = 'tanh'
d_actf = 'tanh'
#
learning_rate = 1e-4
convg_thres = 1e-4
weight_decay = 0.05
#
pretrain_thres = 1e-3
isPretrain = False
#
num_chunks = 2
num_epochs = None
#
data_dir = '../../data/preprocessed/ml-1m-more-features/pp/'
out_dir = '../../out/ml1m/dcmf/'
result_fpath = out_dir+"result.log"
#
total_num_folds = 5
#
at_k = 200
#
train_percent_list = ['95'] #['orig','60','80','95']
#
#load side matrices
#user feature
df_rf = pd.read_csv(data_dir+'rf_all_more.csv')
#movie feature
df_cf = pd.read_csv(data_dir+'cf_all_more1.csv')

print("Completed loading the data")
print("df_rf.shape:  ",df_rf.shape)
print("df_cf.shape: ",df_cf.shape)
print("#")
#as matrices 
orgU = df_rf.as_matrix()
orgV = df_cf.as_matrix()
print("orgU.shape: ",orgU.shape)
print("orgV.shape: ",orgV.shape)
print("#")
#
Uscaler = preprocessing.MaxAbsScaler()
U = Uscaler.fit_transform(orgU)
Vscaler = preprocessing.MaxAbsScaler()
V = Vscaler.fit_transform(orgV)

print("U.shape: ",U.shape)
print("V.shape: ",V.shape)
print("#")
print("U: ",np.min(U),", ", np.max(U),", ", np.median(U))
print("V: ",np.min(V),", ", np.max(V),", ", np.median(V))
print("#")
#
print("")
for train_percent in train_percent_list:
    print("######")
    print("train_percent: ",train_percent)
    run_name = "ml1mk_"+train_percent+"_deepcmf_"
    folds_dir = "folds_"+train_percent 
    #
    dict_fold_prob_at_k = {}
    dict_fold_prob_at_k_train = {}
    dict_fold_prob_at_k_rel = {}
    for fold_num in np.arange(1,total_num_folds+1):
        #
        print("fold#: ",fold_num)
        print("###")
        #central matrix fold 
        orgRtrain = pkl.load(open(data_dir+folds_dir+'/matrix_um_train_fold_'+str(fold_num)+'.pkl','rb'),encoding='latin1')
        Rscaler = preprocessing.MaxAbsScaler()
        Rtrain = Rscaler.fit_transform(orgRtrain)
        Rtest = pkl.load(open(data_dir+folds_dir+'/matrix_um_test_fold_'+str(fold_num)+'.pkl','rb'),encoding='latin1')
        #
        dcmf = deepcmf(learning_rate=learning_rate,\
                        num_epochs=num_epochs,\
                        convg_thres=convg_thres,\
                        num_chunks=num_chunks,\
                        k=k,\
                        kf=kf,\
                        e_actf=e_actf,\
                        d_actf=d_actf,\
                        isPretrain=isPretrain,\
                        pretrain_thres=pretrain_thres,
                        weight_decay=weight_decay)
        dcmf.fit(Rtrain,U,V)
        #
        Rpred, Ue1, Ue2 = dcmf.predict()
        # pkl.dump(Rpred,open(out_dir+run_name+"fold_"+str(fold_num)+"_Rpred.pkl","wb"))
        # pkl.dump(Ue1,open(out_dir+run_name+"fold_"+str(fold_num)+"_Ue1.pkl","wb"))
        # pkl.dump(Ue2,open(out_dir+run_name+"fold_"+str(fold_num)+"_Ue2.pkl","wb"))
        #
        print("Rtrain.shape: ",Rtrain.shape)
        print("Rtrain - min: ",np.min(V),", max: ", np.max(V),", median: ", np.median(V))
        print("Rtest.shape: ",Rtest.shape)
        print("U.shape: ",U.shape)
        print("V.shape: ",V.shape)
        print("#")
        print("Rpred.shape: ",Rpred.shape)
        print("Ue1.shape: ",Ue1.shape)
        print("Ue2.shape: ",Ue2.shape)
        print("#")
        print("Train recall: ")
        prob_at_k_train = dcmf.get_recall_at_k(Rpred,lil_matrix(orgRtrain),at_k)
        print("#")
        print("Test recall: ")
        prob_at_k = dcmf.get_recall_at_k(Rpred,lil_matrix(Rtest),at_k)
        dict_epochs_cost = dcmf.get_epochs_cost()
        dict_fold_prob_at_k_train[fold_num] = prob_at_k_train
        dict_fold_prob_at_k[fold_num] = prob_at_k
        temp_df = pd.DataFrame.from_dict(dict_epochs_cost,orient='index')
        temp_df.to_csv(out_dir+run_name+'epochs_cost_fold_'+str(fold_num)+'.csv',index=False)
        temp_df.plot()
        plt.title('fold: '+str(fold_num))
        plt.savefig(out_dir+run_name+'epochs_cost_fold_'+str(fold_num)+'.jpg')
        print("Result fold#: ",fold_num)
        print("test_r_at_"+str(at_k)+": "+str(prob_at_k[at_k]))
        print("train_r_at_"+str(at_k)+": "+str(prob_at_k_train[at_k]))
        print("#")
    #
    dic_prob_at_k_avg = {}
    dic_prob_at_k_avg_train = {}
    for k in np.arange(at_k):
        prob_values = []
        for prob_at_k in dict_fold_prob_at_k.values():
            prob_values.append(prob_at_k[k+1])
        avg_prob = np.mean(prob_values)
        dic_prob_at_k_avg[k+1] = avg_prob
        #train
        prob_values = []
        for prob_at_k in dict_fold_prob_at_k_train.values():
            prob_values.append(prob_at_k[k+1])
        avg_prob = np.mean(prob_values)
        dic_prob_at_k_avg_train[k+1] = avg_prob
     
    pd.DataFrame.from_dict(dic_prob_at_k_avg,orient='index').to_csv(out_dir+run_name+'recall_at_k_'+str(at_k)+'.csv',index=False)
    pd.DataFrame.from_dict(dic_prob_at_k_avg_train,orient='index').to_csv(out_dir+run_name+'recall_at_k_train_'+str(at_k)+'.csv',index=False)
    #
    pd.DataFrame.from_dict(dict_fold_prob_at_k,orient='index').to_csv(out_dir+run_name+'recall_at_k_'+str(at_k)+'_all_folds.csv',index=False)
    pd.DataFrame.from_dict(dict_fold_prob_at_k_train,orient='index').to_csv(out_dir+run_name+'recall_at_k_'+str(at_k)+'_all_folds_train.csv',index=False)
    
    print("test_r_at_"+str(at_k)+": "+str(dic_prob_at_k_avg[at_k]))
    print("train_r_at_"+str(at_k)+": "+str(dic_prob_at_k_avg_train[at_k]))

    with open(result_fpath,'a') as fout:
        fout.write("learning_rate: "+str(learning_rate)+\
        ", convg_thres: "+str(convg_thres)+\
        ", isPretrain: "+str(isPretrain)+\
        ", k: "+str(k)+\
        ", kf: "+str(kf)+\
        ", e_actf: "+e_actf+\
        ", d_actf: "+d_actf+\
        ", num_epochs: "+str(num_epochs)+\
        ", total_num_folds: "+str(total_num_folds)+\
        ", num_chunks: "+str(num_chunks))
        fout.write("\n")
        fout.write("test_r_at_"+str(at_k)+": "+str(round(dic_prob_at_k_avg[at_k],3)))
        fout.write("\n")
        fout.write("train_r_at_"+str(at_k)+": "+str(round(dic_prob_at_k_avg_train[at_k],3)))
        fout.write("\n")
        fout.write("----")
        fout.write("\n")
    fout.close()
