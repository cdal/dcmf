
import deepcmf_syn_sim4_wrapper as syn_sim4_wrapper
import pprint
import numpy as np
import pickle as pkl
import pandas as pd

class deepcmf_gpyopt():
    """
    deepcmf
    """
    def __init__(self,cur_run_data_dir,num_folds):
        self.cur_run_data_dir = cur_run_data_dir
        self.num_folds = num_folds
        self.pp = pprint.PrettyPrinter()
        self.at_k = 100
        print("#")
        print("Loading data from cur_run_data_dir: ",cur_run_data_dir)
        orgU1 = pd.read_csv(cur_run_data_dir+"gene_patient_sample_score.csv",header=None).as_matrix() #X_13
        #orgU2 = pd.read_csv(cur_run_data_dir+"gene_gene_binary.csv",header=None).as_matrix() #X_14
        orgU2 = pd.read_csv(cur_run_data_dir+"gene_genefeatures.csv",header=None).as_matrix() #X_14
        #orgV1 = pd.read_csv(cur_run_data_dir+"disease_disease_score.csv",header=None).as_matrix() #X_26
        orgV1 = pd.read_csv(cur_run_data_dir+"disease_diseasefeatures.csv",header=None).as_matrix() #X_26
        orgW1 = pd.read_csv(cur_run_data_dir+"patient_features_score.csv",header=None).as_matrix().T #X_53
        orgR_temp_dict = {}
        for fold_num in np.arange(1,num_folds+1):
            Rtrain = pd.read_csv(cur_run_data_dir+'pp/Rtrain_fold_'+str(fold_num)+'_bin.csv',header=None).as_matrix() #X_12_train_fold_1.csv #Rtrain_fold_1_bin.csv
            Rtest = pd.read_csv(cur_run_data_dir+'pp/Rtest_fold_'+str(fold_num)+'_bin.csv',header=None).as_matrix() #X_12_test_fold_1.csv #Rtest_fold_1_bin.csv
            orgR_temp_dict[fold_num] = {"Rtrain":Rtrain,"Rtest":Rtest}
        self.org_data_dict = {"orgU1":orgU1,"orgU2":orgU2,"orgV1":orgV1,"orgW1":orgW1,"orgR":orgR_temp_dict}
        print("#")

    def run_dcmf(self,x):
        print("run_dcmf: ")
        x_params_list = x[0]
        learning_rate = x_params_list[0]
        conv_threshold = x_params_list[1]
        weight_decay = x_params_list[2]
        kf = x_params_list[3]
        #
        dict_params = {"learning_rate":learning_rate,\
                "conv_threshold":conv_threshold,\
                "weight_decay":weight_decay,\
                "kf":kf,\
                "num_batches":1,\
                "max_epochs":200}
        num_train_test_splits = self.num_folds
        cur_run_data_dir = self.cur_run_data_dir
        dict_prob_at_k_avg_train,dict_prob_at_k_avg_test,all_losses = syn_sim4_wrapper.run_deepcmf_syn_simulation_4(cur_run_data_dir,dict_params,num_train_test_splits,self.org_data_dict)
        print("run_dcmf - result: ")
        print("dict_params: ")
        self.pp.pprint(dict_params)
        print("all_losses: ")
        self.pp.pprint(all_losses)
        print("perf: ")
        print("dict_prob_at_k_avg_train[",self.at_k,"]: ",dict_prob_at_k_avg_train[self.at_k])
        print("dict_prob_at_k_avg_test[",self.at_k,"]: ",dict_prob_at_k_avg_test[self.at_k])
        print("#")
        return all_losses,{"avg_prob_at_k_train":dict_prob_at_k_avg_train,"avg_prob_at_k_test":dict_prob_at_k_avg_test}

    # def run_dcmf_gp(self,x):
    #     print("run_dcmf: ")
    #     x_params_list = x[0]
    #     learning_rate = x_params_list[0]
    #     conv_threshold = x_params_list[1]
    #     weight_decay = x_params_list[2]
    #     kf = x_params_list[3]
    #     #
    #     dict_params = {"learning_rate":learning_rate,\
    #             "conv_threshold":conv_threshold,\
    #             "weight_decay":weight_decay,\
    #             "kf":kf,\
    #             "num_batches":1,\
    #             "max_epochs":500}
    #     num_train_test_splits = self.num_folds
    #     cur_run_data_dir = self.cur_run_data_dir
    #     avg_rmse_train,sd_train,avg_rmse_test,sd_test,all_losses = syn_sim4_wrapper.run_deepcmf_syn_simulation_4(cur_run_data_dir,dict_params,num_train_test_splits,self.org_data_dict)
    #     print("run_dcmf - result: ")
    #     print("dict_params: ")
    #     self.pp.pprint(dict_params)
    #     print("all_losses: ")
    #     self.pp.pprint(all_losses)
    #     print("perf: ")
    #     print("avg_rmse_train: ",avg_rmse_train)
    #     print("sd_train: ",sd_train)
    #     print("avg_rmse_test: ",avg_rmse_test)
    #     print("sd_test: ",sd_test)
    #     print("#")
    #     return np.atleast_2d(np.sum(all_losses)),{"avg_rmse_train":avg_rmse_train,"sd_train":sd_train,"avg_rmse_test":avg_rmse_test,"sd_test":sd_test}