import pandas as pd
import numpy as np
import time
#
import torch
from torch.autograd import Variable
from torch.nn import Tanh, Sigmoid, ReLU, LeakyReLU
from torch import nn
#
from aec import autoencoder
#
from deepcmf_syn_sim4 import deepcmf
#
import pickle as pkl
#
from sklearn import preprocessing
import time
#
import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt


def run_deepcmf_syn_simulation_4(cur_run_data_dir,dict_params,num_train_test_splits,org_data_dict):
    #
    learning_rate = dict_params['learning_rate']
    convg_thres = dict_params['conv_threshold']
    weight_decay = dict_params['weight_decay']
    kf = dict_params['kf']
    num_chunks = dict_params['num_batches']
    max_epochs = dict_params['max_epochs']
    #
    k = 100
    at_k = 100 
    e_actf = 'tanh'
    d_actf = 'tanh'
    #
    isPretrain = False
    pretrain_thres = 1e-3
    #
    num_epochs = None
    #
    data_dir = cur_run_data_dir
    #out_dir = '/data/deepcmf_repo/out/syn_data/exp1/'
    #
    total_num_folds = num_train_test_splits
    #debug
    print("current run params:")
    print("#")
    print("learning_rate: ",learning_rate)
    print("convg_thres: ",convg_thres)
    print("weight_decay: ",weight_decay)
    print("kf: ",kf)
    print("num_chunks: ",num_chunks)
    print("#")
    print("k: ",k)
    print("e_actf: ",e_actf)
    print("d_actf: ",d_actf)
    print("#")
    print("isPretrain: ",isPretrain)
    print("pretrain_thres: ",pretrain_thres)
    print("#")
    print("num_epochs: ",num_epochs)
    print("max_epochs: ",max_epochs)
    print("total_num_folds: ",total_num_folds)
    print("#")
    print("data_dir: ",data_dir)
    #print("out_dir: ",out_dir)
    print("#")
    #
    #load side matrices
    # orgU1 = pkl.load(open(data_dir+"X_13.pkl",'rb'))
    # orgU2 = pkl.load(open(data_dir+"X_14.pkl",'rb'))
    # orgV1 = pkl.load(open(data_dir+"X_26.pkl",'rb'))
    # orgW1 = pkl.load(open(data_dir+"X_53.pkl",'rb'))
    #
    orgU1 = org_data_dict["orgU1"]
    orgU2 = org_data_dict["orgU2"]
    orgV1 = org_data_dict["orgV1"]
    orgW1 = org_data_dict["orgW1"]
    #
    # print("orgU1.shape: ",orgU1.shape)
    # print("orgU2.shape: ",orgU2.shape)
    # print("orgV1.shape: ",orgV1.shape)
    # print("orgW1.shape: ",orgW1.shape)
    #
    # Uscaler = preprocessing.MaxAbsScaler()
    # U1 = Uscaler.fit_transform(orgU1)
    # Vscaler = preprocessing.MaxAbsScaler()
    # V1 = Vscaler.fit_transform(orgV1)
    U1 = orgU1
    U2 = orgU2
    V1 = orgV1
    W1 = orgW1
    #
    print("U1.shape: ",U1.shape)
    print("U2.shape: ",U2.shape)
    print("V1.shape: ",V1.shape)
    print("W1.shape: ",W1.shape)
    print("#")
    # print("U1 - min: ",np.min(U1),", max: ", np.max(U1),", med: ", np.median(U1))
    # print("U2 - min: ",np.min(U2),", max: ", np.max(U2),", med: ", np.median(U2))
    # print("V1 - min: ",np.min(V1),", max: ", np.max(V1),", med: ", np.median(V1))
    # print("W1 - min: ",np.min(W1),", max: ", np.max(W1),", med: ", np.median(W1))
    # print("#")
    #
    #run_name = "syn_deepcmf_exp4_"
    #
    dict_fold_rmse_test = {}
    dict_fold_rmse_train = {}
    dict_fold_losses = {}
    for fold_num in np.arange(1,total_num_folds+1):
        dict_epochs_cost = {}
        #
        #print("fold#: ",fold_num)
        #print("###")
        
        # #central matrix fold 
        # orgRtrain = pkl.load(open(data_dir+'/X_12_train_fold_'+str(fold_num)+'.pkl','rb'))
        # #Rscaler = preprocessing.MaxAbsScaler()
        # #Rtrain = Rscaler.fit_transform(orgRtrain)
        # Rtrain = orgRtrain
        # Rtrain_idx = pkl.load(open(data_dir+'/X_12_train_idx_'+str(fold_num)+'.pkl','rb')) 
        # Rtest = pkl.load(open(data_dir+'/X_12_test_fold_'+str(fold_num)+'.pkl','rb'))
        # Rtest_idx = pkl.load(open(data_dir+'/X_12_test_idx_'+str(fold_num)+'.pkl','rb'))
        # Rdoublets = pkl.load(open(data_dir+'/R_doublets_'+str(fold_num)+'.pkl','rb'))

        #central matrix fold
        Rtrain = org_data_dict["orgR"][fold_num]["Rtrain"]
        #Rtrain = orgRtrain
        #Rtrain_idx = org_data_dict["orgR"][fold_num]["Rtrain_idx"]
        Rtest = org_data_dict["orgR"][fold_num]["Rtest"]
        #Rtest_idx = org_data_dict["orgR"][fold_num]["Rtest_idx"]
        #Rdoublets = org_data_dict["orgR"][fold_num]["Rdoublets"]

        #print("Rtrain.shape: ",Rtrain.shape)
        #print("Rtest.shape: ",Rtest.shape)
        #print("len(Rtest_idx): ",len(Rtest_idx))
        #print("Rdoublets.shape: ",Rdoublets.shape)
        #print("Rtrain - min: ",np.min(Rtrain),", max: ", np.max(Rtrain),", med: ", np.median(Rtrain))
        #
        dcmf = deepcmf(learning_rate=learning_rate,\
                        num_epochs=num_epochs,\
                        convg_thres=convg_thres,\
                        num_chunks=num_chunks,\
                        k=k,\
                        kf=kf,\
                        e_actf=e_actf,\
                        d_actf=d_actf,\
                        isPretrain=isPretrain,\
                        pretrain_thres=pretrain_thres,\
                        weight_decay=weight_decay,\
                        max_epochs=max_epochs)
        #print("fit: start")
        s = time.time()
        dcmf.fit(Rtrain,U1,U2,V1,W1.T)
        e = time.time()
        #print("fit: end. Took ",round((e-s)/60.0,2)," mins.")
        #
        Rpred = dcmf.predict()
        #
        #print("#")
        print("Rpred.shape: ",Rpred.shape)
        #print("#")
        #print("get_rmse - train")
        dict_fold_rmse_train[fold_num] = dcmf.get_prob_at_k(Rpred.T,Rtrain.T,at_k) #dcmf.get_rmse(Rpred,orgRtrain,Rtrain_idx,Rdoublets)
        #print("get_rmse - test")
        dict_fold_rmse_test[fold_num] = dcmf.get_prob_at_k(Rpred.T,Rtest.T,at_k) #dcmf.get_rmse(Rpred,Rtest.todense(),Rtest_idx,Rdoublets)
        #print("get_losses - train")
        dict_fold_losses[fold_num] = dcmf.get_losses()
        #
        print("Result fold#: ",fold_num)
        print("prob@"+str(at_k)+" test: ",dict_fold_rmse_test[fold_num][at_k])
        print("prob@"+str(at_k)+" train: ",dict_fold_rmse_train[fold_num][at_k])
        print("Losses train: ",dict_fold_losses[fold_num])
        print("#")
    #
    dic_prob_at_k_avg = {}
    dic_prob_at_k_avg_train = {}
    for k in np.arange(at_k):
        prob_values = []
        for prob_at_k in dict_fold_rmse_test.values():
            prob_values.append(prob_at_k[k+1])
        avg_prob = np.mean(prob_values)
        dic_prob_at_k_avg[k+1] = avg_prob
        #train
        prob_values = []
        for prob_at_k in dict_fold_rmse_train.values():
            prob_values.append(prob_at_k[k+1])
        avg_prob = np.mean(prob_values)
        dic_prob_at_k_avg_train[k+1] = avg_prob


    temp_list = []
    for temp_val in list(dict_fold_losses.values()):
        temp_list.append(list(temp_val[0]))
    temp_arr = np.array(temp_list)
    avg_losses = np.atleast_2d(np.mean(temp_arr,axis=0))
    #
    print("Result current run - avg of 5 folds:")
    print("prob@"+str(at_k)+" test: ",dic_prob_at_k_avg[at_k])
    print("prob@"+str(at_k)+" train: ",dic_prob_at_k_avg_train[at_k])
    print("Losses train: ",avg_losses)
    print("###")
    return dic_prob_at_k_avg_train,dic_prob_at_k_avg,avg_losses


