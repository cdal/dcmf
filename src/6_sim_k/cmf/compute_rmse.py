import pandas as pd
import numpy as np
import time
import random
import pickle as pkl
import os
import random
from scipy.sparse import coo_matrix,lil_matrix
from sklearn.metrics import mean_squared_error

def get_rmse(Rpred,Rtest,Rtest_idx,Rdoublets):
    print("Computing RMSE.")
    print("Rpred.shape: ",Rpred.shape)
    print(Rpred)
    print("Rtest.shape: ",Rtest.shape)
    print(Rtest)
    print("len(Rtest_idx): ",len(Rtest_idx))
    print("type(Rtest_idx): ",type(Rtest_idx))
    print("Rtest_idx.shape: ",Rtest_idx.shape)
    print("Rdoublets.shape: ",Rdoublets.shape)
    #Rtest_bin = Rtest > 0
    Rtest_bin = np.zeros(Rtest.shape)
    for idx in Rtest_idx:
        ij = Rdoublets[idx][0]
        Rtest_bin[ij[0],ij[1]] = 1
    Rtest_bin = Rtest_bin > 0
    nz_list_pred = Rpred[Rtest_bin].tolist()
    print("nz_list_pred.shape: ",len(nz_list_pred))
    nz_list_test = Rtest[Rtest_bin].T.tolist()
    print("nz_list_test.shape: ",len(nz_list_test))
    print("target: ",nz_list_test[:10])
    print("pred: ",nz_list_pred[:10])
    rmse = np.sqrt(mean_squared_error(nz_list_test,nz_list_pred))
    #mse = np.sum(np.absolute(nz_list_test-nz_list_pred))
    print("rmse: ",rmse)
    print("---")
    return rmse


algo = "gcmf"
sim_name = "sim4_k_"

base_dir = "/home/ragu/cdal/deepcmf_mlj/data/preprocessed/syn_data/sim31/"
base_out_dir = "/home/ragu/cdal/deepcmf_mlj/src/6_sim_k/out/"+algo+"/"

num_runs = 1
num_folds = 1


for k in np.arange(20,220,20):
    
    print("###")
    print("K: ",k)
    print("###")

    dict_run_rmse_train = {}
    dict_run_rmse_test = {}
    dict_run_rmse_train_sd = {}
    dict_run_rmse_test_sd = {}


    for cur_run in np.arange(1,num_runs+1):
        print("-----")
        print("cur_run: ",cur_run)
        print("-----")
        train_rmse_list = []
        test_rmse_list = []
        # if cur_run == 5:
        #     num_folds = 1
        # else:
        #     num_folds = 5
        for fold_num in np.arange(1,num_folds+1):
            print("#")
            print("fold_num: ",fold_num)
            print("#")       
            fname_Rpred = base_out_dir+"/"+str(cur_run)+"/"+str(k)+"/RPred_"+str(fold_num)+".csv"
            print("fname_Rpred: ",fname_Rpred)
            fname_Rtest = base_dir+str(cur_run)+"/X_12_test_fold_"+str(fold_num)+".csv"
            fname_Rtrain = base_dir+str(cur_run)+"/X_12_train_fold_"+str(fold_num)+".csv"
            fname_Rtest_idx = base_dir+str(cur_run)+"/X_12_test_idx_"+str(fold_num)+".csv"
            fname_Rtrain_idx = base_dir+str(cur_run)+"/X_12_train_idx_"+str(fold_num)+".csv"
            fname_Rdoublets = base_dir+str(cur_run)+"/R_doublets_"+str(fold_num)+".csv"
            #
            df_Rpred = pd.read_csv(fname_Rpred)
            df_Rtrain = pd.read_csv(fname_Rtrain)
            df_Rtest = pd.read_csv(fname_Rtest)
            df_Rtest_idx = pd.read_csv(fname_Rtest_idx)
            df_Rtrain_idx = pd.read_csv(fname_Rtrain_idx)
            df_Rdoublets = pd.read_csv(fname_Rdoublets)
            #
            Rpred = df_Rpred.values
            Rtrain = df_Rtrain.values
            Rtest = df_Rtest.values
            Rtest_idx = df_Rtest_idx.values
            Rtrain_idx = df_Rtrain_idx.values
            Rdoublets = df_Rdoublets.values
            #
            print("Train:")
            cur_fold_train_rmse = get_rmse(Rpred,Rtrain,Rtrain_idx,Rdoublets)
            print("Test: ")
            cur_fold_test_rmse = get_rmse(Rpred,Rtest,Rtest_idx,Rdoublets)
            #
            train_rmse_list.append(cur_fold_train_rmse)
            test_rmse_list.append(cur_fold_test_rmse)
            #
            print("cur_fold_train_rmse: ",cur_fold_train_rmse)
            print("cur_fold_test_rmse: ",cur_fold_test_rmse)
        avg_cur_run_train_rmse = np.mean(train_rmse_list)
        train_sd = np.std(train_rmse_list) 
        avg_cur_run_test_rmse = np.mean(test_rmse_list)
        test_sd = np.std(test_rmse_list)
        print("#")
        print("cur_run: ",cur_run)
        print("avg_cur_run_train_rmse: ",avg_cur_run_train_rmse," +/- ",train_sd)
        print("avg_cur_run_test_rmse: ",avg_cur_run_test_rmse," +/- ",test_sd)
        dict_run_rmse_train[cur_run] = avg_cur_run_train_rmse
        dict_run_rmse_train_sd[cur_run] = train_sd
        dict_run_rmse_test[cur_run] = avg_cur_run_test_rmse
        dict_run_rmse_test_sd[cur_run] = test_sd

    print("#########")
    print("tech: ",algo)
    print("sim_name: ",sim_name)
    print("#")
    for cur_run in dict_run_rmse_train.keys():
        print("run: ",cur_run,", train RMSE: ",dict_run_rmse_train[cur_run], " +/- ",dict_run_rmse_train_sd[cur_run])
    print("#")
    for cur_run in dict_run_rmse_test.keys():
        print("run: ",cur_run,", test RMSE: ",dict_run_rmse_test[cur_run], " +/- ",dict_run_rmse_test_sd[cur_run])
    print("#########")

    pkl.dump(dict_run_rmse_test,open(base_out_dir+"out_"+algo+"_dict_k_rmse_"+str(k)+".pkl","wb"))

 