import pprint
import pandas as pd
import numpy as np
import pickle as pkl
import time
import itertools
import os

from scipy.sparse import lil_matrix

import sys
sys.path.append("..")

from src.dcmf_bo import dcmf_bo
from src.dcmf import dcmf

data_dir = "/home/ragu/cdal/deepcmf_mlj/data/preprocessed/syn_data/sim31/1/"
out_dir = "/home/ragu/cdal/deepcmf_mlj/src/6_sim_k/out/"

X_12 = pd.read_csv(data_dir+"X_12_train_fold_1.csv").as_matrix() #X_12
X_13 = pd.read_csv(data_dir+"X_13.csv").as_matrix() #X_13
X_42 = pd.read_csv(data_dir+"X_42.csv").as_matrix() #X_14
X_12_test = pd.read_csv(data_dir+"X_12_test_fold_1.csv").as_matrix() #X_14


print("Loaded data:")
print("X_12.shape (g-d) : ",X_12.shape)#g-d
print("X_13.shape (g-gf) : ",X_13.shape)#g-gf
print("X_42.shape (df-d): ",X_42.shape)#df-d
print("#")
print("X_12_test.shape (g-d) : ",X_12_test.shape)#g-d


X_data = {
    "X1":{"1":X_12},\
    "X2":X_13,\
    "X3":X_42}

X_meta = {
    "X1":["e1","e2"],\
    "X2":["e1","e3"],\
    "X3":["e4","e2"]}

G = {
    "e1":["X1","X2"],\
    "e2":["X1","X3"],\
    "e3":["X2"],\
    "e4":["X3"]}

num_folds = 1 #TODO: Make the code work even if num_folds i.e. num_val_sets is 0
X_val = {
    "X1":{"1":lil_matrix(X_12_test)},
}


kf = 0.01
#k = 100
e_actf = "tanh"
d_actf = "tanh"
is_linear_last_enc_layer = False
is_linear_last_dec_layer = False
num_chunks = 1

learning_rate = 0.001
weight_decay = 0.05
max_epochs = 1000
convg_thres = 0.001

is_pretrain=False
pretrain_thres= 0.1
max_pretrain_epochs = 2

val_metric = "rmse"
is_val_transpose = False
at_k = 10

is_gpu = True
gpu_ids = "0,1"

best_criterion = "val" 
num_bo_steps = 200
initial_design_size = 20


dict_k_rmse = {}
for k in np.arange(20,220,20):
    print("#start# k: ",k)
    print("###")
    dcmf_bo_model = dcmf_bo(G, X_data, X_meta,\
            num_chunks=num_chunks,k=int(k), kf=kf, e_actf=e_actf, d_actf=d_actf,\
            learning_rate=None, weight_decay=None, convg_thres=convg_thres, max_epochs=max_epochs,\
            is_gpu=is_gpu,gpu_ids=gpu_ids,is_pretrain=is_pretrain, pretrain_thres=pretrain_thres,\
            max_pretrain_epochs=max_pretrain_epochs,X_val=X_val,val_metric=val_metric,\
            is_val_transpose=is_val_transpose, at_k=at_k, best_criterion=best_criterion,num_bo_steps=num_bo_steps,initial_design_size=initial_design_size,\
            is_linear_last_enc_layer=is_linear_last_enc_layer,is_linear_last_dec_layer=is_linear_last_dec_layer,num_val_sets=num_folds)
    dcmf_bo_model.fit()
    cur_rmse = dcmf_bo_model.out_dict_p_hash_info['val_perf_all_folds_total_avg']
    print("#result# k: ",k," | rmse: ",cur_rmse)
    dict_k_rmse[k] = cur_rmse
    print("#")
    pkl.dump(dcmf_bo_model.out_dict_p_hash_info,open(out_dir+"dict_dcmf_out_k_"+str(k)+".pkl","wb"))

pkl.dump(dict_k_rmse,open(out_dir+"out_dict_k_rmse_dcmf_bo.pkl","wb"))

