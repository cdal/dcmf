
# coding: utf-8

import pandas as pd
import numpy as np
import time
from synthetic_data_sparse import SyntheticDataGeneratorSparse
import random
import deepcmf_syn_sim4_wrapper as syn_sim4_wrapper
from sklearn.model_selection import KFold
from scipy.sparse import lil_matrix
import pickle as pkl
import os
import random

import pprint
pp = pprint.PrettyPrinter()


data_dir = "../../data/preprocessed/syn_data/sim4/"
out_dir = "../../out/syn/sim4/"

num_runs = 1
num_params_samples = 200
num_folds = 1

num_entities = 6
entity_size_list = [1000,2000,200,150,300,250]
k = 100
entity_relation_dict = {"1":["2","3","4"],"2":["6"],"5":["3"]}
sparsity_percentage_list = [0]

load_data = True #True - for loading data from previously generated one


def get_train_test(view_matrices_dict,num_folds,sparse_percent):
    print("get_train_test - start")
    R = view_matrices_dict['X_12']
    #
    num_rows = R.shape[0]
    num_cols = R.shape[1]
    #
    R_doublets = []
    i_nz_list,j_nz_list = lil_matrix(R).nonzero()
    print("intended sparsity: ",sparse_percent)
    print("actual sparsity (% zero): ",100*(1 - (len(i_nz_list)/float(np.prod(R.shape)))))
    print("#")
    for i in np.arange(len(i_nz_list)):
        cur_i = i_nz_list[i]
        cur_j = j_nz_list[i]
        R_doublets.append([cur_i,cur_j])
    R_doublets = np.array(R_doublets)
    #
    # R_doublets = []
    # for i in np.arange(R.shape[0]):
    #     for j in np.arange(R.shape[1]):
    #         R_doublets.append([i,j])
    # R_doublets = np.array(R_doublets)
    #
    #kf = KFold(n_splits=num_folds)
    common_num_test = 25000
    list_tup_train_test_idx_lists = []
    for fold_num in np.arange(1,num_folds+1):
        print("fold_num: ",fold_num)
        all_idx = np.arange(len(list(R_doublets)))
        temp_test_idx_list = list(random.sample(list(all_idx),common_num_test))
        temp_train_idx_list = list(set(all_idx)-set(temp_test_idx_list))
        print("len(temp_train_idx_list): ",len(temp_train_idx_list))
        print("len(temp_test_idx_list): ",len(temp_test_idx_list))
        print("#")
        list_tup_train_test_idx_lists.append((np.array(temp_train_idx_list),np.array(temp_test_idx_list)))
    
    #
    fold_num = 1
    #for train_index, test_index in kf.split(R_doublets):
    for train_index, test_index in list_tup_train_test_idx_lists:        
        R_test = lil_matrix((num_rows,num_cols))
        R_train = R.copy()
        for idx in test_index:
            ij = R_doublets[idx]
            R_test[ij[0],ij[1]] = R[ij[0],ij[1]]
            R_train[ij[0],ij[1]] = 0
        #
        view_matrices_dict["X_12_train_fold_"+str(fold_num)] = R_train
        view_matrices_dict["X_12_test_fold_"+str(fold_num)] = R_test
        view_matrices_dict["X_12_train_idx_"+str(fold_num)] = train_index
        view_matrices_dict["X_12_test_idx_"+str(fold_num)] = test_index
        view_matrices_dict["R_doublets_"+str(fold_num)] = R_doublets
        fold_num+=1
    print("get_train_test - end.")
    return view_matrices_dict


def generate_random_data(num_entities,entity_size_list,k,entity_relation_dict,num_folds,sparse_percent):
    data_generator = SyntheticDataGeneratorSparse(num_entities,entity_size_list,k,entity_relation_dict,sparse_percent)
    view_matrices_dict,entity_factors_dict = data_generator.get_random_non_negative_view_matrices()
    dict_data = get_train_test(view_matrices_dict,num_folds,sparse_percent)
    assert dict_data != None
    return dict_data


def persist_data(cur_run_data_dir,dict_data):
    for vname in dict_data.keys():
        print("Pickling vname: ",vname,"...")
        pkl.dump(dict_data[vname],open(cur_run_data_dir+vname+".pkl",'wb'))
        print("CSVing... ")
        if isinstance(dict_data[vname],np.ndarray):
            pd.DataFrame(dict_data[vname]).to_csv(cur_run_data_dir+vname+".csv",index=None)
        else:
            pd.DataFrame(dict_data[vname].todense()).to_csv(cur_run_data_dir+vname+".csv",index=None)


def generate_random_params():
    dict_params = {"learning_rate":random.uniform(1e-6,1e-4),\
                    "conv_threshold":1e-4,\
                    "#conv_threshold":random.uniform(1e-5,1e-4),\
                    #"weight_decay":0.05,\
                    "weight_decay":random.uniform(0.05,0.5),\
                    #"kf":0.1,\
                    "kf":random.uniform(0.1,0.5),\
                    "num_batches":1,\
                    "max_epochs":200}
    return dict_params


def run_deepcmf(cur_run_data_dir,dict_params,num_folds,org_data_dict):
    cur_rmse_train,cur_sd_train, cur_rmse_test, cur_sd_test, all_losses = syn_sim4_wrapper.run_deepcmf_syn_simulation_4(cur_run_data_dir,dict_params,num_folds,org_data_dict)
    return cur_rmse_train,cur_sd_train, cur_rmse_test, cur_sd_test


def get_best_rmse(dict_params_run_rmse):
    #dict_params = {"learning_rate":1e-4,"conv_threshold":1e-3,"weight_decay":0.05}
    #dict_best_rmse = {"rmse_train":-1,"rmse_test":-1,"params":dict_params}
    temp_list_key = []
    temp_list_test_rmse = []
    for run_key in dict_params_run_rmse.keys():
        temp_list_key.append(run_key)
        temp_list_test_rmse.append(dict_params_run_rmse[run_key]['rmse_test'])
    max_rmse_i = np.argmin(np.array(temp_list_test_rmse))
    max_rmse_key = temp_list_key[max_rmse_i]
    dict_best_rmse = dict_params_run_rmse[max_rmse_key]    
    return dict_best_rmse


def get_avg_rmse(dict_run_best_rmse):
    temp_list_train_rmse = []
    temp_list_test_rmse = []
    for run_key in dict_run_best_rmse.keys():
        temp_list_train_rmse.append(dict_run_best_rmse[run_key]['rmse_train'])
        temp_list_test_rmse.append(dict_run_best_rmse[run_key]['rmse_test'])
    dict_avg_rmse = {"avg_rmse_train":np.mean(temp_list_train_rmse),                    
                    "sd_rmse_train":np.std(temp_list_train_rmse),                    
                    "avg_rmse_test":np.mean(temp_list_test_rmse),                    
                    "sd_rmse_test":np.std(temp_list_test_rmse)}    
    return dict_avg_rmse


dict_run_best_rmse = {}
dict_run_all_rmse = {}
for i in np.arange(1,num_runs+1):
    run_s = time.time()
    print("run #",i," - start")
    print("###################")
    if load_data:
        #Loading previous data
        cur_run_data_dir = data_dir+str(i)+'/'
        print("Loading data from cur_run_data_dir: ",cur_run_data_dir)
    else:
        #generate data
        print(">>> Generating data...")
        print("sparsity_percentage ",sparsity_percentage_list[i-1])
        dict_data = generate_random_data(num_entities,entity_size_list,k,entity_relation_dict,num_folds,sparsity_percentage_list[i-1])
        #persist data
        cur_run_data_dir = data_dir+str(i)+'/'
        if not os.path.isdir(cur_run_data_dir):
           os.makedirs(cur_run_data_dir)
        print(">>> Persisting data in ",cur_run_data_dir)
        persist_data(cur_run_data_dir,dict_data)
        print("#")
    #Load data once to avoid repeated loading in each of the runs
    orgU1 = pkl.load(open(cur_run_data_dir+"X_13.pkl",'rb'))
    orgU2 = pkl.load(open(cur_run_data_dir+"X_14.pkl",'rb'))
    orgV1 = pkl.load(open(cur_run_data_dir+"X_26.pkl",'rb'))
    orgW1 = pkl.load(open(cur_run_data_dir+"X_53.pkl",'rb'))
    orgR_temp_dict = {}
    for fold_num in np.arange(1,num_folds+1):
        orgRtrain = pkl.load(open(cur_run_data_dir+'/X_12_train_fold_'+str(fold_num)+'.pkl','rb'))
        Rtrain = orgRtrain
        Rtrain_idx = pkl.load(open(cur_run_data_dir+'/X_12_train_idx_'+str(fold_num)+'.pkl','rb')) 
        Rtest = pkl.load(open(cur_run_data_dir+'/X_12_test_fold_'+str(fold_num)+'.pkl','rb'))
        Rtest_idx = pkl.load(open(cur_run_data_dir+'/X_12_test_idx_'+str(fold_num)+'.pkl','rb'))
        Rdoublets = pkl.load(open(cur_run_data_dir+'/R_doublets_'+str(fold_num)+'.pkl','rb'))
        orgR_temp_dict[fold_num] = {"orgRtrain":orgRtrain,"Rtrain_idx":Rtrain_idx,"Rtest":Rtest,"Rtest_idx":Rtest_idx,"Rdoublets":Rdoublets}
    org_data_dict = {"orgU1":orgU1,"orgU2":orgU2,"orgV1":orgV1,"orgW1":orgW1,"orgR":orgR_temp_dict}
    #sample parameters
    dict_params_run_rmse = {}
    for j in np.arange(1,num_params_samples+1):
        ps_s = time.time()
        print("parameter sampling #",j," - start")
        print("-----------------------------------")
        dict_params = generate_random_params()
        #run
        print("Running DeepCMF...")
        cur_rmse_train, cur_sd_train, cur_rmse_test, cur_sd_test = run_deepcmf(cur_run_data_dir,dict_params,num_folds,org_data_dict)
        dict_params_run_rmse[j] = {"rmse_train":cur_rmse_train,"sd_train":cur_sd_train,"rmse_test":cur_rmse_test,"sd_test":cur_sd_test,"params":dict_params}
        ps_e = time.time()
        print("dict_params_run_rmse[j]: ")
        print("#")
        pp.pprint(dict_params_run_rmse[j])
        print("#")
        print("parameter sampling #",j," - end. Took ",round(ps_e-ps_s,2)," secs.")
        print("------------------------------------------------------------")
    dict_cur_run_best_rmse = get_best_rmse(dict_params_run_rmse)
    print("dict_cur_run_best_rmse: ")
    print(dict_cur_run_best_rmse)
    dict_run_best_rmse[i] = dict_cur_run_best_rmse
    dict_run_all_rmse[i] = dict_params_run_rmse
    run_e = time.time()
    print("run #",i," - end. Took ",round(run_e-run_s,2)," secs.")
    print("###############################################")
# #print result
# dict_avg_rmse = get_avg_rmse(dict_run_best_rmse)
# print("Simulation #2, Train RMSE: ",dict_avg_rmse["avg_rmse_train"]," +/- ",dict_avg_rmse["sd_rmse_train"]) #unused?
# print("Simulation #2, Test RMSE: ",dict_avg_rmse["avg_rmse_test"]," +/- ",dict_avg_rmse["sd_rmse_test"])
#persist result
#pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"simulation_4_runs_"+str(num_runs)+"_params_sampling_"+str(num_params_samples)+"_best_rmse.json")
#pd.DataFrame.from_dict(dict_run_all_rmse).to_json(out_dir+"simulation_4_runs_"+str(num_runs)+"_params_sampling_"+str(num_params_samples)+"_all_rmse.json")

pd.DataFrame.from_dict(dict_run_all_rmse).to_json(out_dir+"sim4_random.json")

import pprint
pp = pprint.PrettyPrinter()
print("result: ")
print("###")
print("dict_run_best_rmse: ")
print("#")
pp.pprint(dict_run_best_rmse)
print("#")
print("dict_run_all_rmse: ")
print("#")
pp.pprint(dict_run_all_rmse)