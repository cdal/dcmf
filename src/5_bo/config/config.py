import logging

#Synthetic data generator
working_dir_path ="/../../../dcmf"
log_dir = working_dir_path + "/logs"
log_format = "%(asctime)s %(filename)s %(lineno)d %(levelname)s %(message)s"
log_level = logging.DEBUG
