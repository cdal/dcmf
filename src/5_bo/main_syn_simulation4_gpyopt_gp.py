import numpy as np
import time
import os
import pandas as pd
import pprint
import syn_simulation4_gpyopt_wrapper as syn_sim4_gpyopt_wrapper

data_dir = "../../data/preprocessed/syn_data/sim4/"
out_dir = "../../out/syn/sim4/"
num_runs_idxs = [1] #selected runs that were reported in the paper - in the previous syn exp (random search hyperopt) experiments

num_params_samples = 200
num_folds = 1
model_type = "gp"
#model_type = "mtgp"
W_pool = False
W = None

#starts here...
pp = pprint.PrettyPrinter()
dict_run_best_rmse = {}
dict_run_all_rmse = {}
for i in num_runs_idxs: #np.arange(1,num_runs+1):
    run_s = time.time()
    print("run #",i," - start")
    cur_run_data_dir = data_dir+str(i)+'/'
    if not os.path.isdir(cur_run_data_dir):
        raise Exception("Missing data_dir: "+str(data_dir))
    print(">>> Loading data from ",cur_run_data_dir)
    print("#")
    print("Performing BO for hyperparameter selection and running dCMF.")
    ps_s = time.time()
    print("BO hyperopt run #",i," - start")
    print("-----------------------------------")        
    dict_run_all_rmse = syn_sim4_gpyopt_wrapper.dcmf_bo(cur_run_data_dir,num_params_samples,num_folds,model_type,W_pool,W) 
                                              # return format: 
                                              # dict_params_run_rmse[j] = {"rmse_train":cur_rmse_train,"sd_train":cur_sd_train,"rmse_test":cur_rmse_test,"sd_test":cur_sd_test,"params":dict_params}
                                              # where
                                              # dict_params = {"learning_rate":1e-6,\
                                              # "conv_threshold":1e-4,\
                                              # "weight_decay":0.05,\
                                              # "kf":0.1,\
                                              # "num_batches":1,\
                                              # "max_epochs":500}
                                              # and 
                                              # j is the run idx
    ps_e = time.time()
    print("BO hyperopt run #",i," - end. Took ",round(ps_e-ps_s,2)," secs.")
    print("------------------------------------------------------------")
    #dict_cur_run_best_rmse = get_best_rmse(dict_params_run_rmse)
    # print("dict_cur_run_best_rmse: ")
    # pp.pprint(dict_cur_run_best_rmse)
    # dict_run_best_rmse[i] = dict_cur_run_best_rmse
    # dict_run_all_rmse[i] = dict_params_run_rmse
    run_e = time.time()
    print("run #",i," - end. Took ",round(run_e-run_s,2)," secs.")
    print("###############################################")

#persist result
#pd.DataFrame.from_dict(dict_run_best_rmse).to_json(out_dir+"simulation_32_runs_"+str(num_runs_idxs)+"_params_sampling_"+str(num_params_samples)+"_best_rmse.json")
#pd.DataFrame.from_dict(dict_run_all_rmse).to_json(out_dir+"simulation_4_runs_"+str(num_runs_idxs)+"_params_sampling_"+str(num_params_samples)+"_all_rmse_GP.json")

pd.DataFrame.from_dict(dict_run_all_rmse).to_json(out_dir+"sim4_gp.json")


print("result: ")
print("###")
print("dict_run_all_rmse: ")
print("#")
pp.pprint(dict_run_all_rmse)

