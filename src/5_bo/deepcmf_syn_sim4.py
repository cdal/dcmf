import pandas as pd
import numpy as np
import time
#
import torch
from torch.autograd import Variable
from torch.nn import Tanh, Sigmoid, ReLU, LeakyReLU
from torch import nn
#
from aec import autoencoder
#
import time
#
import pickle as pkl
from scipy.sparse import coo_matrix,lil_matrix
from sklearn.metrics import mean_squared_error
import os


class deepcmf():
    """
    deepcmf
    """
    def __init__(self,learning_rate=0.001,num_epochs=None,convg_thres=1e-3,num_chunks=1,k=10,kf=0.5,e_actf='lrelu',d_actf='lrelu',isPretrain=False, pretrain_thres=None,weight_decay=None,max_epochs=2000):
        print("deepcmf: init")
        #params
        self.learning_rate = learning_rate
        self.num_epochs = num_epochs
        self.convg_thres = convg_thres
        self.pretrain_thres = pretrain_thres
        self.num_chunks = num_chunks
        self.k = k
        self.kf = kf
        self.e_actf = e_actf
        self.d_actf = d_actf
        #factors
        self.Ue1 = None
        self.Ue2 = None
        self.Rpred = None
        self.cost_list = None
        self.all_losses = None
        self.isPretrain = isPretrain
        self.weight_decay=weight_decay
        self.max_epochs = max_epochs
        os.environ["CUDA_VISIBLE_DEVICES"]="1"

    def __get_k_list(self,n):
        k_list = []
        while True:
            k1 = int(n * self.kf)
            if k1 > self.k:
                k_list.append(k1)
                n = k1
            else:
                k_list.append(self.k)
                break
        return k_list

    def __get_actf_list(self,k_list):
        actf_list_e = []
        actf_list_d = []
        for k in k_list:
            actf_list_e.append(self.e_actf) 
            actf_list_d.append(self.d_actf)
        actf_list = actf_list_e+actf_list_d 
        return actf_list

    def __is_converged(self,prev_cost,cost,convg_thres):
        diff = (prev_cost - cost)
        if (abs(diff)) < convg_thres:
            return True

    def __get_pretrain_weights(self,\
        X1,X2,X3,X4,\
        x1_k_list,x1_actf_list,\
        x2_k_list,x2_actf_list,\
        x3_k_list,x3_actf_list,\
        x4_k_list,x4_actf_list,
        learning_rate,\
        num_epochs,\
        pretrain_thres
        ):
        print("###")
        print("Pretraining - start")
        aec1 = autoencoder(X1.shape[0],x1_k_list,x1_actf_list,None)#CUDA
        aec1.cuda()
        print("#")
        aec2 = autoencoder(X2.shape[0],x2_k_list,x2_actf_list,None)
        aec2.cuda()
        print("#")
        aec3 = autoencoder(X3.shape[0],x3_k_list,x3_actf_list,None)
        aec3.cuda()
        print("#")
        aec4 = autoencoder(X4.shape[0],x4_k_list,x4_actf_list,None)
        aec4.cuda() 
        #train
        criterion = nn.MSELoss()
        model_params = list(aec1.parameters())+list(aec2.parameters())+list(aec3.parameters())+list(aec4.parameters())
        optimizer = torch.optim.Adam(model_params, lr=learning_rate, weight_decay=self.weight_decay)
        epoch = 0
        prev_cost = 0
        while True:
            if num_epochs is not None:
                if epoch > num_epochs:
                    break
            else:
                if epoch > self.max_epochs:
                    break
            s = time.time()
            cost_epoch = 0
            cost_X1 = 0
            cost_X2 = 0
            cost_X3 = 0
            cost_X4 = 0
            X1_rec,Ue1 = aec1(X1.transpose(1,0)) #.cuda())#CUDA
            X2_rec,Ue2 = aec2(X2.transpose(1,0)) #.cuda())
            X3_rec,Ue3 = aec3(X3.transpose(1,0)) #.cuda())
            X4_rec,Ue4 = aec4(X4.transpose(1,0)) #.cuda()) 
            #
            # print("Sanity check: ")
            # print("X1.shape: ",X1.shape)
            # print("X1_rec.shape: ",X1_rec.shape)
            loss_X1 = criterion(X1_rec.transpose(1,0),X1) #.cuda())
            loss_X2 = criterion(X2_rec.transpose(1,0),X2) #.cuda())
            loss_X3 = criterion(X3_rec.transpose(1,0),X3) #.cuda())
            loss_X4 = criterion(X4_rec.transpose(1,0),X4) #.cuda())          
            #
            loss = loss_X1 + loss_X2 + loss_X3 + loss_X4
            #
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            #
            cost_epoch += loss.item()
            cost_X1 += loss_X1.item()
            cost_X2 += loss_X2.item()
            cost_X3 += loss_X3.item()
            cost_X4 += loss_X4.item()
            e = time.time()
            print("###")
            print("pretrain-epoch: ",epoch," cost: ",cost_epoch," Took ",round(e-s,1)," secs.")
            print("loss_X1: ",cost_X1)
            print("loss_X2: ",cost_X2)
            print("loss_X3: ",cost_X3)
            print("loss_X4: ",cost_X4)
            print("#")
            epoch+=1
            if self.__is_converged(prev_cost,cost_epoch,pretrain_thres):
                break
            prev_cost = cost_epoch   
        print("###")
        print("debug: ")
        print("#")
        print("X1: ")
        print(X1)
        print("X1_rec: ")
        print(X1_rec)
        print("###")
        print("X2: ")
        print(X2)
        print("X2_rec: ")
        print(X2_rec)
        print("###")
        print("X3: ")
        print(X3)
        print("X3_rec: ")
        print(X3_rec)
        print("###")
        print("X4: ")
        print(X4)
        print("X4_rec: ")
        print(X4_rec)
        print("###")
        print("Pretraining - end")  
        aec1_params_list = aec1.parameters()
        aec2_params_list = aec2.parameters()
        aec3_params_list = aec3.parameters()
        aec4_params_list = aec4.parameters()
        return aec1_params_list,\
               aec2_params_list,\
               aec3_params_list,\
               aec4_params_list 

    # def __get_nz_idx(self,X1,U):
    #     x1_nz_i = []
    #     x1_nz_j = []
    #     for i in np.arange(U.shape[1]): 
    #         for j in np.arange(U.shape[0]):
    #             x1_nz_i.append(i)
    #             x1_nz_j.append(j)
    #     #
    #     nz_i, nz_j = coo_matrix(X1).nonzero()
    #     count = 0
    #     for idx in np.arange(len(nz_i)):
    #         i = nz_i[idx]
    #         j = nz_j[idx]
    #         if (i >= U.shape[1]) and (j <= U.shape[0]):
    #             x1_nz_i.append(i)
    #             x1_nz_j.append(j)
    #             count+=1
    #     return list(x1_nz_i),list(x1_nz_j)

    def fit(self,R,U1,U2,V,W):
        # print("Factorizing.")
        # print("R.shape: ",R.shape)
        # print("U1.shape: ",U1.shape)
        # print("U2.shape: ",U2.shape)
        # print("V.shape: ",V.shape)
        # print("W.shape: ",W.shape)
        # print("#")
        X1 = Variable(torch.from_numpy(np.hstack((U1,U2,R)).T).float(),requires_grad=False).cuda() #CUDA  #.cuda(),requires_grad=False) #CUDA 
        X2 = Variable(torch.from_numpy(np.vstack((V.T,R))).float(),requires_grad=False).cuda()   
        X3 = Variable(torch.from_numpy(np.vstack((U1,W.T))).float(),requires_grad=False).cuda()
        X4 = Variable(torch.from_numpy(V).float(),requires_grad=False).cuda()
        X5 = Variable(torch.from_numpy(U2).float(),requires_grad=False).cuda()
        X6 = Variable(torch.from_numpy(W).float(),requires_grad=False).cuda()
        
        # print("X1.shape: ",X1.shape)
        # print("X2.shape: ",X2.shape)
        # print("X3.shape: ",X3.shape)
        # print("X4.shape: ",X4.shape)
        # print("X5.shape: ",X5.shape)
        # print("X6.shape: ",X6.shape)
        # print("#")

        X1_chunks_list = torch.chunk(X1,self.num_chunks,dim=1)
        X2_chunks_list = torch.chunk(X2,self.num_chunks,dim=1)
        X3_chunks_list = torch.chunk(X3,self.num_chunks,dim=1)
        X4_chunks_list = torch.chunk(X4,self.num_chunks,dim=1)
        X5_chunks_list = torch.chunk(X5,self.num_chunks,dim=1)
        X6_chunks_list = torch.chunk(X6,self.num_chunks,dim=1)

        # print("X1_chunks_list[0].shape: (u+n,m/#) ",X1_chunks_list[0].shape)
        # print("X2_chunks_list[0].shape: (v+m,n/#) ",X2_chunks_list[0].shape)
        # print("X3_chunks_list[0].shape: (m,u1/#) ",X3_chunks_list[0].shape)
        # print("X4_chunks_list[0].shape: (n,v/#) ",X4_chunks_list[0].shape)
        # print("X5_chunks_list[0].shape: (m,u2/#) ",X5_chunks_list[0].shape)
        # print("X6_chunks_list[0].shape: (u1,w/#) ",X6_chunks_list[0].shape)
        # print("#")

        orgU1 = Variable(torch.from_numpy(U1).float(),requires_grad=False).cuda() #CUDA  #.cuda(),requires_grad=False) #CUDA 
        orgU2 = Variable(torch.from_numpy(U2).float(),requires_grad=False).cuda()
        orgV = Variable(torch.from_numpy(V).float(),requires_grad=False).cuda()
        orgR = Variable(torch.from_numpy(R).float(),requires_grad=False).cuda()
        orgW = Variable(torch.from_numpy(W).float(),requires_grad=False).cuda()

        m,n = orgR.shape[0],orgR.shape[1]
        u1 = orgU1.shape[1]
        u2 = orgU2.shape[1]
        v = orgV.shape[1]
        w = orgW.shape[1]

        # print("m: ",m,", n: ",n,", u1: ",u1,", u2: ",u2,", v: ",v,", w: ",w)
        # print("Var U1.shape: (m,u1) ",orgU1.shape)
        # print("Var U2.shape: (m,u2) ",orgU2.shape)
        # print("Var V.shape: (n,v) ",orgV.shape)
        # print("Var R.shape: (m,n) ",orgR.shape)
        # print("Var W.shape: (u1,w) ",orgW.shape)
        # print("#")

        # #init row & col bias for each of the views
        # UrB = Variable(torch.zeros(1,u),requires_grad=True).cuda() #.cuda(),requires_grad=True)
        # UcB = Variable(torch.zeros(m,1),requires_grad=True).cuda()
        # #
        # VrB = Variable(torch.zeros(1,v),requires_grad=True).cuda() #m,u #r bias => col vector 
        # VcB = Variable(torch.zeros(n,1),requires_grad=True).cuda()      #c bias => row vector
        # #
        # RrB = Variable(torch.zeros(1,n),requires_grad=True).cuda()
        # RcB = Variable(torch.zeros(m,1),requires_grad=True).cuda()
        # #error
        # Uerr = Variable(torch.zeros(m,u),requires_grad=True).cuda()
        # Verr = Variable(torch.zeros(n,v),requires_grad=True).cuda()
        # Rerr = Variable(torch.zeros(m,n),requires_grad=True).cuda()

        # print("UrB.shape: ",UrB.shape)
        # print("UcB.shape: ",UcB.shape)
        # print("VrB.shape: ",VrB.shape)
        # print("VcB.shape: ",VcB.shape)
        # print("RrB.shape: ",RrB.shape)
        # print("RcB.shape: ",RcB.shape)
        # print("Uerr.shape: ",Uerr.shape)
        # print("Verr.shape: ",Verr.shape)
        # print("Rerr.shape: ",Rerr.shape)
        # print("#")

        #k,kf,e_actf,d_actf
        x1_k_list = self.__get_k_list(X1.shape[0]) #[800, 200]
        x2_k_list = self.__get_k_list(X2.shape[0]) #[500, 200]
        x3_k_list = self.__get_k_list(X3.shape[0]) #[800, 200]
        x4_k_list = self.__get_k_list(X4.shape[0]) #[500, 200] 
        x5_k_list = self.__get_k_list(X5.shape[0])
        x6_k_list = self.__get_k_list(X6.shape[0])
        #
        x1_actf_list = self.__get_actf_list(x1_k_list)
        x2_actf_list = self.__get_actf_list(x2_k_list)
        x3_actf_list = self.__get_actf_list(x3_k_list)
        x4_actf_list = self.__get_actf_list(x4_k_list)
        x5_actf_list = self.__get_actf_list(x5_k_list)
        x6_actf_list = self.__get_actf_list(x6_k_list)
        #
        # print("aec1: ")
        # print("input_shape: ",X1.shape[0])
        # print("k_list: ",x1_k_list)
        # print("actf_list: ",x1_actf_list)
        # print("---")
        # print("aec2: ")
        # print("input_shape: ",X2.shape[0])
        # print("k_list: ",x2_k_list)
        # print("actf_list: ",x2_actf_list)
        # print("---")
        # print("aec3: ")
        # print("input_shape: ",X3.shape[0])
        # print("k_list: ",x3_k_list)
        # print("actf_list: ",x3_actf_list)
        # print("---")
        # print("aec4: ")
        # print("input_shape: ",X4.shape[0])
        # print("k_list: ",x4_k_list)
        # print("actf_list: ",x4_actf_list)
        # print("#")
        # print("---")
        # print("aec5: ")
        # print("input_shape: ",X5.shape[0])
        # print("k_list: ",x5_k_list)
        # print("actf_list: ",x5_actf_list)
        # print("#")
        # print("---")
        # print("aec6: ")
        # print("input_shape: ",X6.shape[0])
        # print("k_list: ",x6_k_list)
        # print("actf_list: ",x6_actf_list)
        # print("#")

        aec1_params_list = None
        aec2_params_list = None
        aec3_params_list = None
        aec4_params_list = None
        aec5_params_list = None
        aec6_params_list = None
        #pretraining - start

        #print("isPretrain: ",self.isPretrain)
        #print("#")
        if self.isPretrain:
            aec1_params_list,\
            aec2_params_list,\
            aec3_params_list,\
            aec4_params_list = self.__get_pretrain_weights(X1,X2,X3,X4,\
                                                            x1_k_list,x1_actf_list,\
                                                            x2_k_list,x2_actf_list,\
                                                            x3_k_list,x3_actf_list,\
                                                            x4_k_list,x4_actf_list,
                                                            self.learning_rate,\
                                                            self.num_epochs,\
                                                            self.pretrain_thres)

        torch.cuda.empty_cache() 
        #pretraining - end   
        # Create AEC with pretraining weights
        #print("aec1: ")
        aec1 = autoencoder(X1.shape[0],x1_k_list,x1_actf_list,aec1_params_list) #CUDA
        aec1.cuda()
        #print("#")
        #print("aec2: ")
        aec2 = autoencoder(X2.shape[0],x2_k_list,x2_actf_list,aec2_params_list)
        aec2.cuda()
        #print("#")
        #print("aec3: ")
        aec3 = autoencoder(X3.shape[0],x3_k_list,x3_actf_list,aec3_params_list)
        aec3.cuda()
        #print("#")
        #print("aec4: ")
        aec4 = autoencoder(X4.shape[0],x4_k_list,x4_actf_list,aec4_params_list)
        aec4.cuda()
        #print("#")
        #print("aec5: ")
        aec5 = autoencoder(X5.shape[0],x5_k_list,x5_actf_list,aec5_params_list)
        aec5.cuda()
        #print("#")
        #print("aec6: ")
        aec6 = autoencoder(X6.shape[0],x6_k_list,x6_actf_list,aec6_params_list)
        aec6.cuda()
        #print("#")

        #
        #training
        criterion = nn.MSELoss()
        model_params = list(aec1.parameters())+list(aec2.parameters())+list(aec3.parameters())+list(aec4.parameters())+list(aec5.parameters())+list(aec6.parameters()) #+[UrB,UcB,VrB,VcB,RrB,RcB,Uerr,Verr,Rerr]
        optimizer = torch.optim.Adam(model_params, lr=self.learning_rate, weight_decay=self.weight_decay)
        #
        #main loop
        self.dict_epoch_cost = {}
        epoch = 1
        prev_cost = 0
        while True:
            if self.num_epochs is not None and epoch > self.num_epochs:
                break
            else:
                if epoch > self.max_epochs:
                    break
            s = time.time()
            #indexes to manage batches
            prev_m_i = 0
            prev_n_i = 0
            prev_u1_i = 0
            prev_u2_i = 0
            prev_v_i = 0
            prev_w_i = 0
            #holds factors learnt batchwise
            Ue1_chunks_list = []
            Ue2_chunks_list = []
            Ue3_chunks_list = []
            Ue4_chunks_list = []
            Ue5_chunks_list = []
            Ue6_chunks_list = []
            #total loss for this epoch
            cost_epoch = 0
            cost_X1 = 0
            cost_X2 = 0
            cost_X3 = 0
            cost_X4 = 0
            cost_X5 = 0
            cost_X6 = 0
            cost_U1 = 0
            cost_U2 = 0
            cost_V = 0
            cost_R = 0
            cost_W = 0
            #batches loop
            for i in np.arange(self.num_chunks):
                #print("#### Processing Chunk: ",i)
                X1_chunk = X1_chunks_list[i]
                X2_chunk = X2_chunks_list[i]
                X3_chunk = X3_chunks_list[i]
                X4_chunk = X4_chunks_list[i]
                X5_chunk = X5_chunks_list[i]
                X6_chunk = X6_chunks_list[i]
                # print("X1_chunk.shape: ",X1_chunk.shape)
                # print("X2_chunk.shape: ",X2_chunk.shape)
                # print("X3_chunk.shape: ",X3_chunk.shape)
                # print("X4_chunk.shape: ",X4_chunk.shape)
                # print("X5_chunk.shape: ",X5_chunk.shape)
                # print("X6_chunk.shape: ",X6_chunk.shape)
                # print("#")
                #
                m_i = prev_m_i + X1_chunk.shape[1]
                n_i = prev_n_i + X2_chunk.shape[1]
                u1_i = prev_u1_i + X3_chunk.shape[1]
                u2_i = prev_u2_i + X5_chunk.shape[1]
                v_i = prev_v_i + X4_chunk.shape[1]
                w_i = prev_w_i + X6_chunk.shape[1]
                # print("# prev_m_i: ",prev_m_i)
                # print("# prev_n_i: ",prev_n_i)
                # print("# prev_u1_i: ",prev_u1_i)
                # print("# prev_u2_i: ",prev_u2_i)
                # print("# prev_v_i: ",prev_v_i)
                # print("# prev_w_i: ",prev_w_i)
                # print("#")
                # print("# m_i: ",m_i)
                # print("# n_i: ",n_i)
                # print("# u1_i: ",u1_i)
                # print("# u2_i: ",u2_i)
                # print("# v_i: ",v_i)
                # print("# w_i: ",w_i)
                # print("#")   
                X1_chunk_rec,Ue1_chunk = aec1(X1_chunk.transpose(1,0))#CUDA #.cuda())#CUDA
                X2_chunk_rec,Ue2_chunk = aec2(X2_chunk.transpose(1,0))
                X3_chunk_rec,Ue3_chunk = aec3(X3_chunk.transpose(1,0))
                X4_chunk_rec,Ue4_chunk = aec4(X4_chunk.transpose(1,0))
                X5_chunk_rec,Ue5_chunk = aec5(X5_chunk.transpose(1,0))
                X6_chunk_rec,Ue6_chunk = aec6(X6_chunk.transpose(1,0))
                # print("Ue1_chunk.shape: (m_i,k) ",Ue1_chunk.shape)
                # print("Ue2_chunk.shape: (n_i,k) ",Ue2_chunk.shape)
                # print("Ue3_chunk.shape: (u1_i,k) ",Ue3_chunk.shape)
                # print("Ue4_chunk.shape: (v_i,k) ",Ue4_chunk.shape)
                # print("Ue5_chunk.shape: (u2_i,k) ",Ue5_chunk.shape)
                # print("Ue6_chunk.shape: (w_i,k) ",Ue6_chunk.shape)
                # print("#")
                # print("X1_chunk_rec.shape: ",X1_chunk_rec.shape)
                # print("X2_chunk_rec.shape: ",X2_chunk_rec.shape)
                # print("X3_chunk_rec.shape: ",X3_chunk_rec.shape)
                # print("X4_chunk_rec.shape: ",X4_chunk_rec.shape)
                # print("X5_chunk_rec.shape: ",X5_chunk_rec.shape)
                # print("X6_chunk_rec.shape: ",X6_chunk_rec.shape)
                # print("#")
                Ue1_chunks_list.append(Ue1_chunk)
                Ue2_chunks_list.append(Ue2_chunk)
                Ue3_chunks_list.append(Ue3_chunk)
                Ue4_chunks_list.append(Ue4_chunk)
                Ue5_chunks_list.append(Ue5_chunk)
                Ue6_chunks_list.append(Ue6_chunk)
                #
                U1_chunk_rec = Ue1_chunk.mm(Ue3_chunk.transpose(1,0))
                U2_chunk_rec = Ue1_chunk.mm(Ue5_chunk.transpose(1,0))
                                # +\
                                # UrB[:,prev_u_i:u_i]+\
                                # UcB[prev_m_i:m_i,:]+\
                                # Uerr[prev_m_i:m_i,prev_u_i:u_i])
                V_chunk_rec = Ue2_chunk.mm(Ue4_chunk.transpose(1,0))
                                # +\
                                # VrB[:,prev_v_i:v_i]+\
                                # VcB[prev_n_i:n_i,:]+\
                                # Verr[prev_n_i:n_i,prev_v_i:v_i]
                R_chunk_rec = Ue1_chunk.mm(Ue2_chunk.transpose(1,0))
                                # +\
                                # RrB[:,prev_n_i:n_i]+\
                                # RcB[prev_m_i:m_i,:]+\
                                # Rerr[prev_m_i:m_i,prev_n_i:n_i]
                W_chunk_rec = Ue3_chunk.mm(Ue6_chunk.transpose(1,0))
                # print("# U1_chunk_rec.shape: ",U1_chunk_rec.shape)
                # print("# U2_chunk_rec.shape: ",U2_chunk_rec.shape)
                # print("# V_chunk_rec.shape: ",V_chunk_rec.shape)
                # print("# R_chunk_rec.shape: ",R_chunk_rec.shape)
                # print("# W_chunk_rec.shape: ",W_chunk_rec.shape)
                # print("#")
                U1_chunk = orgU1[prev_m_i:m_i,prev_u1_i:u1_i]
                U2_chunk = orgU2[prev_m_i:m_i,prev_u2_i:u2_i]
                V_chunk = orgV[prev_n_i:n_i,prev_v_i:v_i]
                R_chunk = orgR[prev_m_i:m_i,prev_n_i:n_i]
                W_chunk = orgW[prev_u1_i:u1_i,prev_w_i:w_i]
                # print("# U1_chunk.shape: ",U1_chunk.shape)
                # print("# U2_chunk.shape: ",U2_chunk.shape)
                # print("# V_chunk.shape: ",V_chunk.shape)
                # print("# R_chunk.shape: ",R_chunk.shape)
                # print("# W_chunk.shape: ",W_chunk.shape)
                # print("#")
                prev_m_i = m_i
                prev_n_i = n_i
                prev_u1_i = u1_i
                prev_u2_i = u2_i
                prev_v_i = v_i
                prev_w_i = w_i
                #loss
                #
                loss_X1 = criterion(X1_chunk_rec.transpose(1,0),X1_chunk)#.cuda())
                loss_X2 = criterion(X2_chunk_rec.transpose(1,0),X2_chunk)#.cuda())
                loss_X3 = criterion(X3_chunk_rec.transpose(1,0),X3_chunk)#.cuda())
                loss_X4 = criterion(X4_chunk_rec.transpose(1,0),X4_chunk)#.cuda())
                loss_X5 = criterion(X5_chunk_rec.transpose(1,0),X5_chunk)#.cuda())
                loss_X6 = criterion(X6_chunk_rec.transpose(1,0),X6_chunk)#.cuda())
                loss_U1 = criterion(U1_chunk_rec,U1_chunk)
                loss_U2 = criterion(U2_chunk_rec,U2_chunk)
                loss_V = criterion(V_chunk_rec,V_chunk)
                loss_W = criterion(W_chunk_rec,W_chunk)
                # nz_i, nz_j = coo_matrix(R_chunk.cpu().data.numpy()).nonzero()
                # print("R loss #nnz: ",len(nz_i))
                # loss_R = criterion(R_chunk_rec[nz_i, nz_j],R_chunk[nz_i, nz_j])
                loss_R = criterion(R_chunk_rec,R_chunk)
                loss = loss_X1 + loss_X2 + loss_X3 + loss_X4 + loss_X5 + loss_X6 + loss_U1 + loss_U2 + loss_V + loss_R + loss_W 
                # ===================backward====================
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()
                #
                cost_epoch += loss.item()
                cost_X1 += loss_X1.item()
                cost_X2 += loss_X2.item()
                cost_X3 += loss_X3.item()
                cost_X4 += loss_X4.item()
                cost_X5 += loss_X5.item()
                cost_X6 += loss_X6.item()
                cost_U1 += loss_U1.item()
                cost_U2 += loss_U2.item()
                cost_V += loss_V.item()
                cost_R += loss_R.item()
                cost_W += loss_W.item()
            self.dict_epoch_cost[epoch] = cost_epoch
            e = time.time()
            # print("###")
            # print("epoch: ",epoch," cost: ",cost_epoch," Took ",round(e-s,1)," secs.")
            # print("loss_X1: ",cost_X1)
            # print("loss_X2: ",cost_X2)
            # print("loss_X3: ",cost_X3)
            # print("loss_X4: ",cost_X4)
            # print("loss_X5: ",cost_X5)
            # print("loss_X6: ",cost_X6)
            # print("loss_U1: ",cost_U1)
            # print("loss_U2: ",cost_U2)
            # print("loss_V: ",cost_V)
            # print("loss_R: ",cost_R)
            # print("loss_W: ",cost_W)
            # print("#")
            epoch+=1
            if self.__is_converged(prev_cost,cost_epoch,self.convg_thres):
                break
            prev_cost = cost_epoch
        self.Ue1 = torch.cat(Ue1_chunks_list,dim=0)
        self.Ue2 = torch.cat(Ue2_chunks_list,dim=0)
        self.Rpred = self.Ue1.mm(self.Ue2.transpose(1,0)).cpu().data.numpy() #(self.Ue1.mm(self.Ue2.transpose(1,0))+RrB+RcB+Rerr).cpu().data.numpy()
        self.all_losses =  np.atleast_2d(np.array([cost_X1,cost_X2,cost_X3,cost_X4,cost_X5,cost_X6,cost_U1,cost_U2,cost_V,cost_R,cost_W]))
        # print("--debug start--")            
        # print("Ue1.shape: ",self.Ue1.shape)
        # print("Ue2.shape: ",self.Ue2.shape)
        # print("Rpred.shape: ",self.Rpred.shape)
        # print("---")
        # print("Ue1: ")
        # print(self.Ue1)
        # print("---")
        # print("Ue1: ")
        # print(self.Ue2)
        # print("---")
        # print("Rpred: ")
        # print(self.Rpred)
        # print("---")
        # print("R: ")
        # print(R)
        # print("--debug end--")
        return self

    def predict(self):
        #print("Predicting the central matrix.")
        return self.Rpred

    def get_losses(self):
        return self.all_losses  

    def get_rmse(self,Rpred,Rtest,Rtest_idx,Rdoublets):
        # print("Computing RMSE.")
        # print("Rpred.shape: ",Rpred.shape)
        # print("Rtest.shape: ",Rtest.shape)
        # print("len(Rtest_idx): ",len(Rtest_idx))
        # print("Rdoublets.shape: ",Rdoublets.shape)
        #Rtest_bin = Rtest > 0
        Rtest_bin = np.zeros(Rtest.shape)
        for idx in Rtest_idx:
            ij = Rdoublets[idx]
            Rtest_bin[ij[0],ij[1]] = 1
        Rtest_bin = Rtest_bin > 0
        nz_list_pred = Rpred[Rtest_bin].tolist()
        #print("nz_list_pred.shape: ",len(nz_list_pred))
        nz_list_test = Rtest[Rtest_bin].T.tolist()
        #print("nz_list_test.shape: ",len(nz_list_test))
        rmse = np.sqrt(mean_squared_error(nz_list_test,nz_list_pred))
        #mse = np.sum(np.absolute(nz_list_test-nz_list_pred))
        #print("rmse: ",rmse)
        #print("---")
        return rmse

    def get_epochs_cost(self):
        return self.dict_epoch_cost

    def get_prob_at_k(self,Rpred,Rtest,at_k):
        print("get_prob_at_"+str(at_k)+": ")
        print("Rpred.shape: ",Rpred.shape)
        print("Rtest.shape: ",Rtest.shape)
        test_rank_list = []
        for ij in np.argwhere(Rtest):
            i = ij[0]
            j = ij[1]
            cur_row_rank = (-Rpred[i]).argsort()
            test_rank_list.append(int(np.squeeze(np.argwhere(cur_row_rank == j)))+1)
        #
        dict_cum_prob_at_k = {}
        test_rank_array = np.array(test_rank_list)
        num_test = float(len(test_rank_list))
        for k in np.arange(1,at_k+1):
            num_match = float(np.sum(test_rank_array == k))
            cur_prob = num_match/num_test
            print("k: ",k," num_match: ",num_match," num_test: ",num_test," cur_prob: ",cur_prob)
            if k == 1:
                dict_cum_prob_at_k[k] = cur_prob
            else:
                dict_cum_prob_at_k[k] = dict_cum_prob_at_k[k-1] + cur_prob
        #
        return dict_cum_prob_at_k

    def get_recall_at_k(self,Rpred,Rtest,at_k):
        print("---")
        print("get_recall_at_"+str(at_k)+": ")
        print("Rpred.shape: ",Rpred.shape)
        print("Rtest.shape: ",Rtest.shape)
        num_rows = Rtest.shape[0]
        num_cols = Rtest.shape[1]
        dict_recall_at_k = {}
        dict_k_miss_user = {}
        for k in np.arange(1,at_k+1):
            recall_at_k_list = []
            num_rows_without_test_entries = 0
            for i in np.arange(num_rows):
                #pred
                cur_row_pred_idx_topk = np.argpartition(self.Rpred[i], -k)[-k:] #(-Rpred[i]).argsort()[:k]
                #target
                cur_row_target = Rtest[i]
                cur_row_target_idx = np.argwhere(cur_row_target).ravel()
                #recall
                num_match = len(set(cur_row_pred_idx_topk).intersection(cur_row_target_idx))
                num_total = len(cur_row_target_idx)
                if num_total > 0:
                    recall_at_k = num_match/float(num_total)
                    if k == at_k:
                        print("k: ",k," i: ",i," num_match: ",num_match," num_total: ",num_total," recall_at_k: ",recall_at_k)
                    recall_at_k_list.append(recall_at_k)
                else:
                    if k == at_k:
                        print("k: ",k," i: ",i," num_match: ",num_match," num_total: ",num_total," recall_at_k: NA")    
                    num_rows_without_test_entries+=1
            if num_rows_without_test_entries > 0:
                dict_k_miss_user[k] = num_rows_without_test_entries
            dict_recall_at_k[k] = np.mean(recall_at_k_list)
        #
        print("--debug start--")
        print("num_rows_without_test_entries:")
        for k in dict_k_miss_user.keys():
            print("k: ",k, " #users: ",dict_k_miss_user[k])
        print("--debug end--")
        return dict_recall_at_k
