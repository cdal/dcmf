import GPy
import GPyOpt
import numpy as np
import sys

#sample function that returns the loss/blackbox function value

f_forrester = GPyOpt.objective_examples.experiments1d.forrester() #min val 0.78

def fn_1input_1output(x):
	return GPyOpt.objective_examples.experiments1d.twohumps().f(x)


def fn1(x):
    return (2*x)**2

def fn2(x):
	print("x.shape: ",x.shape)
	y1 = GPyOpt.objective_examples.experiments1d.forrester().f(x)
	print("y.shape: ",y1.shape)
	return y1

def fn_multi_op(x):
	y1 = GPyOpt.objective_examples.experiments1d.forrester().f(x)
	y2 = GPyOpt.objective_examples.experiments1d.forrester(sd=0.1).f(x)
	print("fn_multi_op: ")
	print("x.shape: ",x.shape)
	try:
		if x.shape[0] > 100:
			raise Exception("inner")
	except:
		tb = sys.exc_info()[2]
		raise Exception("outer").with_traceback(tb)
	print("type(y1): ",type(y1))
	print("y1.shape: ",y1.shape)
	print("y2.shape: ",y2.shape)
	print("np.concatenate(y1,y2).shape: ",np.concatenate((y1,y2),axis=1).shape)
	return np.concatenate((y1,y2),axis=1)


def fn_multi_op_multi_ip(x):
	y1 = GPyOpt.objective_examples.experiments2d.sixhumpcamel().f(x)
	y2 = GPyOpt.objective_examples.experiments2d.sixhumpcamel(sd = 0.1).f(x)
	print("fn_multi_op_multi_ip: ")
	print("x: ",x)
	print("x.shape: ",x.shape)
	try:
		if x.shape[0] > 100:
			raise Exception("inner")
	except:
		tb = sys.exc_info()[2]
		raise Exception("outer").with_traceback(tb)
	print("type(y1): ",type(y1))
	print("y1: ",y1)
	print("y1.shape: ",y1.shape)
	print("y2.shape: ",y2.shape)
	print("np.concatenate(y1,y2).shape: ",np.concatenate((y1,y2),axis=1).shape)
	return np.concatenate((y1,y2),axis=1)